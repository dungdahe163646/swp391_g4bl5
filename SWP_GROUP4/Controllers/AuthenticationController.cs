﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Common;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Extensions;
using SWP_GROUP4.Models;
using SWP_GROUP4.Services;
using SWP_GROUP4.ViewModels.User;
using System.Diagnostics;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace SWP_GROUP4.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly FBeautyDbContext _context;
        private readonly IConfiguration _config;
        private readonly IUserServices _userServices;
        private readonly IEmailServices _emailServices;

        public AuthenticationController(
            ILogger<AuthenticationController> logger,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            FBeautyDbContext context,
            IUserServices userServices,
            IEmailServices emailServices,
            IConfiguration config)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
            _userServices = userServices;
            _emailServices = emailServices;
            _config = config;
        }

        public IActionResult IsExist(string Email)
        {
            return Json(_userServices.IsEmailExist(Email));
        }

        public IActionResult Index()
        {
            ViewBag.IsLogin = true;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return Redirect("/Home");
            }
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Redirect("/Authentication/Index");
        }

        public async Task<IActionResult> VerifyEmail()
        {
            return View();
        }

        public async Task<IActionResult> ForgotPassword()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ForgotPassword(UserForgotPasswordViewModel userForgotPasswordViewModel)
        {
            if (!ModelState.IsValid)
                return View("forgotpassword", userForgotPasswordViewModel);
            var userId = _userServices.GetUserByEmail(userForgotPasswordViewModel.Email).Id;
            string token = await _userServices.GenerateResetPasswordToken(userId);
            string url = _config["URL"] + "/Authentication/ConfirmResetPassword?token=" + token;
            string title = "[Important!] Yêu cầu thiết lập lại mật khẩu";
            string content = $"Ai đó đã yêu cầu thiết lập lại mật khẩu bằng email của bạn, nếu đó là bạn, vui lòng nhấn vào liên kết sau để xác nhận thiết lập lại mật khẩu: " + url;
            _emailServices.Send(title, content, userForgotPasswordViewModel.Email);
            return View("ResetPassword", userForgotPasswordViewModel.Email);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> ChangePassword(UserChangePasswordViewModel userChangePasswordViewModel)
        {
            if (!ModelState.IsValid)
                return View("ChangePassword", userChangePasswordViewModel);
            if (!PasswordChecker.Check(userChangePasswordViewModel.Password))
            {
                ModelState.AddModelError("password", "Password must contains uppercase, lowercase, number and special character, at least 8 characters.");
                return View("ChangePassword", userChangePasswordViewModel);
            }
            var userEmail = User.FindFirstValue(ClaimTypes.Email);
            var user = _userServices.GetUserByEmail(userEmail);
            string title = "[Important!] Yêu cầu đổi mật khẩu";
            Random r = new Random();
            string OTP = "FBeauty - " + r.Next(100000, 999999);
            string content = $"Mã xác nhận của bạn là: {OTP}, vui lòng không cung cấp mã này cho bất kỳ ai, mã của bạn có hiệu lực trong 5 phút.";
            user.OTP = OTP;
            user.OTPExpireAt = DateTime.Now.AddMinutes(5);
            await _context.SaveChangesAsync();
            _emailServices.Send(title, content, user.Email);
            return View("ConfirmChangePassword", new UserConfirmOTPViewModel { Password = userChangePasswordViewModel.Password });
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> ConfirmChangePassword(UserConfirmOTPViewModel userConfirmOTPViewModel)
        {
            if (_userServices.IsOTPNullOrExpire(userConfirmOTPViewModel.OTP))
                return View("NullOrExpired");
            var stringId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(stringId);
            await _userServices.ChangePassword(userId, userConfirmOTPViewModel.Password);
            var userEmail = User.FindFirstValue(ClaimTypes.Email);
            string title = "Đổi mật khẩu cho FBeauty";
            string content = $"Bạn vừa đổi mật khẩu thành công, giờ đây bạn có thể sử dụng mật khẩu mới";
            _emailServices.Send(title, content, userEmail);
            return Redirect("/Authentication/Index");
        }


        public async Task<ActionResult> ConfirmResetPassword(string token)
        {
            token = Request.QueryString.Value.Replace("?token=", "");
            if (_userServices.IsResetPasswordTokenNullOrExpire(token))
                return View("NullOrExpired");

            var user = _userServices.GetUserByResetPasswordToken(token);
            string newPassword = await _userServices.ResetPassword(user.Id);
            string title = "Thiết lập lại mật khẩu";
            string content = $"Bạn đã xác nhận thiết lập lại mật khẩu. Mật khẩu mới của bạn là: {newPassword}";
            _emailServices.Send(title, content, user.Email);
            return View("ConfirmResetPassword", user.Email);
        }

        [HttpPost]
        public async Task<IActionResult> Register(AuthenticationViewModel userRegister)
        {
            ViewBag.IsLogin = false;
            if (ModelState.IsValid)
            {
                var existingUser = await _userManager.FindByNameAsync(userRegister.UserName);

                if (existingUser != null)
                {
                    ModelState.AddModelError("username", $"Username '{userRegister.UserName}' already exists.");
                }

                var user = new ApplicationUser
                {
                    Email = userRegister.Email,
                    UserName = userRegister.UserName,
                    FirstName = userRegister.FirstName,
                    LastName = userRegister.LastName,
                    Address = "",
                    BirthDate = new DateTime(1999, 9, 9),
                    About = "Nothing to say",
                    EmailConfirmed = false,
                    PhoneNumber = userRegister.PhoneNumber,
                    Image = "/img/profile/avatar_default.jpg"
                };

                var existingEmail = await _userManager.FindByEmailAsync(userRegister.Email);

                if (existingEmail == null)
                {
                    if (userRegister.Password != userRegister.RePassword)
                    {
                        ModelState.AddModelError("repassword", "Password mismatch!");
                    }
                    else
                    {
                        if (PasswordChecker.Check(userRegister.Password))
                        {
                            var res = await _userManager.CreateAsync(user, userRegister.Password);

                            if (res.Succeeded)
                            {
                                await _userManager.AddToRoleAsync(user, "User");
                            }
                            else
                            {
                                ModelState.AddModelError("password", "Password must contains uppercase, lowercase, number and special character.");
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("password", "Password must contains uppercase, lowercase, number and special character, at least 8 characters.");
                        }

                    }
                }
                else
                {
                    ModelState.AddModelError("email", $"Email {userRegister.Email} already exists.");
                }
            }
            if (ModelState.IsValid && ModelState.ErrorCount == 0)
            {
                //return Redirect("/VerifyEmail");
                return Redirect("/Home");
            }
            else
            {
                return View("Index", userRegister);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Login(AuthenticationViewModel userLogin)
        {
            ViewBag.IsLogin = true;
            ModelState.Remove("FirstName");
            ModelState.Remove("LastName");
            ModelState.Remove("UserName");
            ModelState.Remove("PhoneNumber");
            ModelState.Remove("RePassword");
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(userLogin.Email);
                if (user != null)
                {
                    var result = await _signInManager.PasswordSignInAsync(user, userLogin.Password, false, false);
                    if (result.Succeeded)
                    {
                        return Redirect("/Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("email", "Email doesn't exist in system");
                    return View("Index", userLogin);
                }
                ModelState.AddModelError("password", "Wrong password!");
                return View("Index", userLogin);
            }
            return View("Index", userLogin);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Extensions;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.Category;
using SWP_GROUP4.ViewModels.Product;
using SWP_GROUP4.ViewModels.Service;
using X.PagedList;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SWP_GROUP4.Controllers
{
    [Authorize(Roles = "PAdmin")]
    public class PDashboardController : Controller
    {
        private readonly FBeautyDbContext _context;
        public PDashboardController(FBeautyDbContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            Dictionary<string, int> CategoriesInfo = await _context.Products
                    .Include(pc => pc.Category)
                    .GroupBy(pc => pc.Category.Name)
                    .Select(g => new
                    {
                        category = g.Key,
                        quantity = g.Count()
                    })
                    .OrderByDescending(m => m.quantity)
                    .ToDictionaryAsync(el => el.category, el => el.quantity);

            Dictionary<string, double> ProductRates = await _context.Rates
                    .Include(r => r.Product)
                    .GroupBy(r => r.Product.Name)
                    .Select(mr => new
                    {
                        product = mr.Key,
                        rating = mr
                        .Average(r => r.Rating)
                    })
                    .Take(3)
                    .OrderByDescending(m => m.rating)
                    .ToDictionaryAsync(el => el.product.StringShortener(15), el => el.rating);
            ViewBag.ProductRates = ProductRates;
            ViewBag.CategoriesInfo = CategoriesInfo;
            return View();
        }

        public async Task<IActionResult> ImageSizeTooLarge()
        {
            return View();
        }

        public async Task<IActionResult> InvalidFormat()
        {
            return View();
        }

        public async Task<ViewResult> ProductManagement(string sortOrder, string currentFilter, string searchString, int? page)
        {
            IQueryable<Product> Products = _context.Products.Include(p => p.Company);
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                Products = Products.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    Products = Products.OrderByDescending(p => p.Name);
                    break;
                default:
                    Products = Products.OrderBy(p => p.Name);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(Products.ToPagedList(pageNumber, pageSize));
        }

        public async Task<ViewResult> CategoryManagement(string sortOrder, string currentFilter, string searchString, int? page)
        {
            IQueryable<Category> Categories = _context.Categories;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                Categories = Categories.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    Categories = Categories.OrderByDescending(p => p.Name);
                    break;
                default:
                    Categories = Categories.OrderBy(p => p.Name);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(Categories.ToPagedList(pageNumber, pageSize));
        }

        public async Task<ViewResult> ServiceManagement(string sortOrder, string currentFilter, string searchString, int? page)
        {
            IQueryable<SpaTreatmentPlan> STPs = _context.SpaTreatmentPlans.Include(p => p.Company);
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                STPs = STPs.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    STPs = STPs.OrderByDescending(p => p.Name);
                    break;
                default:
                    STPs = STPs.OrderBy(p => p.Name);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(STPs.ToPagedList(pageNumber, pageSize));
        }

        public async Task<IActionResult> DeactiveProduct([FromQuery(Name = "Id")] string id)
        {
            Guid productId = Guid.Parse(id);
            Product p = await _context.Products.FindAsync(productId);
            p.Status = !p.Status;
            await _context.SaveChangesAsync();
            return Redirect("/PDashboard/ProductManagement");
        }

        public async Task<IActionResult> DeactiveCategory([FromQuery(Name = "Id")] string id)
        {
            Guid categoryId = Guid.Parse(id);
            Category c = await _context.Categories.FindAsync(categoryId);
            c.Status = !c.Status;
            await _context.SaveChangesAsync();
            return Redirect("/PDashboard/CategoryManagement");
        }

        public async Task<IActionResult> DeactiveService([FromQuery(Name = "Id")] string id)
        {
            Guid productId = Guid.Parse(id);
            SpaTreatmentPlan stp = await _context.SpaTreatmentPlans.FindAsync(productId);
            stp.Status = !stp.Status;
            await _context.SaveChangesAsync();
            return Redirect("/PDashboard/ServiceManagement");
        }

        public async Task<IActionResult> AddOrEditProduct([FromQuery(Name = "Id")] string? id)
        {
            ProductFormViewModel pvm;
            if (!String.IsNullOrEmpty(id))
            {
                Guid productId = Guid.Parse(id);
                Product p = await _context.Products.FindAsync(productId);
                pvm = new()
                {
                    Id = p.Id,
                    Name = p.Name,
                    ShortDescription = p.ShortDescription,
                    FullDescription = p.FullDescription,
                    Price = p.Price,
                    Images = p.Images,
                    CompanyId = p.CompanyId,
                    CategoryId = p.CategoryId
                };
            }
            else
            {
                pvm = new();
                pvm.Id = new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
            }
            List<Company> companies = await _context.Companies.ToListAsync();
            Dictionary<Guid, string> companySelectors = new();
            foreach (var item in companies)
            {
                companySelectors.Add(item.Id, item.Name);
            }
            List<Category> categories = await _context.Categories.ToListAsync();
            Dictionary<Guid, string> categorySelectors = new();
            foreach (var item in categories)
            {
                categorySelectors.Add(item.Id, item.Name);
            }
            ViewBag.CompanySelectors = companySelectors;
            ViewBag.CategorySelectors = categorySelectors;
            return View(pvm);
        }

        public async Task<IActionResult> AddOrEditService([FromQuery(Name = "Id")] string? id)
        {
            ServiceFormViewModel svm;
            if (!String.IsNullOrEmpty(id))
            {
                Guid serviceId = Guid.Parse(id);
                SpaTreatmentPlan p = await _context.SpaTreatmentPlans.FindAsync(serviceId);
                svm = new()
                {
                    Id = p.Id,
                    Name = p.Name,
                    ShortDescription = p.ShortDescription,
                    FullDescription = p.FullDescription,
                    Price = p.Price,
                    Images = p.Images,
                    CompanyId = p.CompanyId
                };
            }
            else
            {
                svm = new();
                svm.Id = new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
            }
            List<Company> companies = await _context.Companies.ToListAsync();
            Dictionary<Guid, string> companySelectors = new();
            foreach (var item in companies)
            {
                companySelectors.Add(item.Id, item.Name);
            }
            ViewBag.CompanySelectors = companySelectors;
            return View(svm);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrEditProduct(ProductFormViewModel productAddViewModel, IFormFile productImage)
        {
            // check model state of image manually
            ModelState.Remove("Images");
            if (ModelState.IsValid)
            {
                if (productImage.ContentType.ToLower() != "image/jpg" &&
                    productImage.ContentType.ToLower() != "image/jpeg" &&
                    productImage.ContentType.ToLower() != "image/pjpeg" &&
                    productImage.ContentType.ToLower() != "image/gif" &&
                    productImage.ContentType.ToLower() != "image/x-png" &&
                    productImage.ContentType.ToLower() != "image/png")
                {
                    return RedirectToAction("InvalidFormat");
                }
                else if (productImage.Length >= 2097152 || productImage.Length == 0)
                {
                    return RedirectToAction("ImageSizeTooLarge");
                }
                else
                {
                    string pattern = "[^a-zA-Z0-9]";
                    var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/product");
                    var fileName = "product" + Regex.Replace(DateTime.Now.ToString(), pattern, "") + Path.GetExtension(productImage.FileName);
                    var filePath = Path.Combine(imageFolderPath, fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        productImage.CopyTo(stream);
                    }

                    if (productAddViewModel.Id == new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))
                    {
                        Product p = new Product()
                        {
                            Id = Guid.NewGuid(),
                            Name = productAddViewModel.Name,
                            ShortDescription = productAddViewModel.ShortDescription,
                            FullDescription = productAddViewModel.FullDescription,
                            Images = "/img/product/" + fileName,
                            Price = productAddViewModel.Price,
                            Discount = 25,
                            Quantity = 100,
                            IsCombo = true,
                            ReleasedDate = DateTime.Now,
                            Status = true,
                            CompanyId = productAddViewModel.CompanyId,
                            CategoryId = productAddViewModel.CategoryId
                        };
                        await _context.Products.AddAsync(p);
                    }
                    else
                    {
                        Product p = await _context.Products.FirstOrDefaultAsync(p => p.Id == productAddViewModel.Id);
                        p.Name = productAddViewModel.Name;
                        p.ShortDescription = productAddViewModel.ShortDescription;
                        p.FullDescription = productAddViewModel.FullDescription;
                        p.Images = "/img/product/" + fileName;
                        p.Price = productAddViewModel.Price;
                        p.CompanyId = productAddViewModel.CompanyId;
                        p.CategoryId = productAddViewModel.CategoryId;
                    }

                    await _context.SaveChangesAsync();
                    return Redirect("/PDashboard/Index");
                }
            }
            else
            {
                List<Company> companies = await _context.Companies.ToListAsync();
                Dictionary<Guid, string> companySelectors = new();
                foreach (var item in companies)
                {
                    companySelectors.Add(item.Id, item.Name);
                }
                List<Category> categories = await _context.Categories.ToListAsync();
                Dictionary<Guid, string> categorySelectors = new();
                foreach (var item in categories)
                {
                    categorySelectors.Add(item.Id, item.Name);
                }
                ViewBag.CompanySelectors = companySelectors;
                ViewBag.CategorySelectors = categorySelectors;
                return View(productAddViewModel);
            }
        }

        public async Task<IActionResult> AddOrEditCategory([FromQuery(Name = "Id")] string? id)
        {
            CategoryFormViewModel cvm;
            if (!String.IsNullOrEmpty(id))
            {
                Guid categoryId = Guid.Parse(id);
                Category c = await _context.Categories.FindAsync(categoryId);
                cvm = new()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description
                };
            }
            else
            {
                cvm = new();
                cvm.Id = new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
            }
            return View(cvm);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrEditCategory(CategoryFormViewModel categoryFormViewModel)
        {
            if (ModelState.IsValid)
            {
                if (categoryFormViewModel.Id == new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))
                {
                    Category c = new Category()
                    {
                        Id = Guid.NewGuid(),
                        Name = categoryFormViewModel.Name,
                        Description = categoryFormViewModel.Description,
                        Image = "",
                        Status = true
                    };
                    await _context.Categories.AddAsync(c);
                }
                else
                {
                    Category c = await _context.Categories.FirstOrDefaultAsync(p => p.Id == categoryFormViewModel.Id);
                    c.Name = categoryFormViewModel.Name;
                    c.Description = categoryFormViewModel.Description;
                    c.Image = "";
                }

                await _context.SaveChangesAsync();
                return Redirect("/PDashboard/Index");

            }
            else
            {
                return View(categoryFormViewModel);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddOrEditService(ServiceFormViewModel serviceAddViewModel, IFormFile serviceImage)
        {
            // check model state of image manually
            ModelState.Remove("Images");
            if (ModelState.IsValid)
            {
                if (serviceImage.ContentType.ToLower() != "image/jpg" &&
                    serviceImage.ContentType.ToLower() != "image/jpeg" &&
                    serviceImage.ContentType.ToLower() != "image/pjpeg" &&
                    serviceImage.ContentType.ToLower() != "image/gif" &&
                    serviceImage.ContentType.ToLower() != "image/x-png" &&
                    serviceImage.ContentType.ToLower() != "image/png")
                {
                    return RedirectToAction("InvalidFormat");
                }
                else if (serviceImage.Length >= 2097152 || serviceImage.Length == 0)
                {
                    return RedirectToAction("ImageSizeTooLarge");
                }
                else
                {

                    string pattern = "[^a-zA-Z0-9]";
                    var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/service");
                    var fileName = "service" + Regex.Replace(DateTime.Now.ToString(), pattern, "") + Path.GetExtension(serviceImage.FileName);
                    var filePath = Path.Combine(imageFolderPath, fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        serviceImage.CopyTo(stream);
                    }
                    if (serviceAddViewModel.Id == new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))
                    {
                        SpaTreatmentPlan stp = new SpaTreatmentPlan()
                        {
                            Id = Guid.NewGuid(),
                            Name = serviceAddViewModel.Name,
                            ShortDescription = serviceAddViewModel.ShortDescription,
                            FullDescription = serviceAddViewModel.FullDescription,
                            Images = "/img/service/" + fileName,
                            Price = serviceAddViewModel.Price,
                            Discount = 25,
                            Duration = 180,
                            ReleasedDate = DateTime.Now,
                            Status = true,
                            CompanyId = serviceAddViewModel.CompanyId
                        };
                        await _context.SpaTreatmentPlans.AddAsync(stp);
                    }
                    else
                    {
                        SpaTreatmentPlan s = await _context.SpaTreatmentPlans.FirstOrDefaultAsync(p => p.Id == serviceAddViewModel.Id);
                        s.Name = serviceAddViewModel.Name;
                        s.ShortDescription = serviceAddViewModel.ShortDescription;
                        s.FullDescription = serviceAddViewModel.FullDescription;
                        s.Images = "/img/service/" + fileName;
                        s.Price = serviceAddViewModel.Price;
                        s.CompanyId = serviceAddViewModel.CompanyId;
                    }

                    await _context.SaveChangesAsync();
                    return Redirect("/PDashboard/Index");
                }
            }
            else
            {
                List<Company> companies = await _context.Companies.ToListAsync();
                Dictionary<Guid, string> companySelectors = new();
                foreach (var item in companies)
                {
                    companySelectors.Add(item.Id, item.Name);
                }
                ViewBag.CompanySelectors = companySelectors;
                return View(serviceAddViewModel);
            }
        }
    }
}


﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using System.Data;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class OrderController : Controller
    {
        private readonly ILogger<OrderController> _logger;
        private readonly FBeautyDbContext _context;
        public OrderController(ILogger<OrderController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        public IActionResult Order()
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (rawUserIds == null)
            {
                return Redirect($"/Authentication/Index");
            }
            var order = _context.Orders.Where(u => u.UserId.ToString() == rawUserIds).ToList();
            List<ProductOrder> productOrders = new List<ProductOrder>();
            foreach (var item in order)
            {
                var productorder = _context.ProductOrders.Where(u => u.OrderId == item.Id).ToList();
                productOrders.AddRange(productorder);
            }
            List<Product> products = new List<Product>();
            foreach (var item in productOrders)
            {
                var product = _context.Products.Where(u => u.Id == item.ProductId).ToList();
                products.AddRange(product);
            }
            ViewBag.Products = products;
            ViewBag.orders = order;
            return View();
            //string role = CheckRole();
            //if (role == "VENDOR")
            //{
            //    ViewBag.vendor = "vendor";
            //    return View();
            //}
            //if (role == "USER")
            //{
            //    ViewBag.vendor = "customer";
            //    return View();
            //}
            //return NotFound();
        }
        public string CheckRole()
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            var roleid = _context.UserRoles.FirstOrDefault(u => u.UserId == userId);
            var role = _context.Roles.FirstOrDefault(u => u.Id == roleid.RoleId);
            return role.NormalizedName;
        }

        public IActionResult Cancel(string id)
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (rawUserIds == null)
            {
                return Redirect($"Authentication/Login");
            }
            var order = _context.Orders.FirstOrDefault(u => u.Id.ToString() == id);
            if (order != null)
            {
                order.PaymentStatus = false;
                _context.SaveChanges();
            }
            return RedirectToAction("Order", "Order");
        }

    }
}
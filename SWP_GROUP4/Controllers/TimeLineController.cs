﻿using Microsoft.AspNetCore.Mvc;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using System;
using System.Linq;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class TimelineController : Controller
    {
        private readonly ILogger<SpaController> _logger;
        private readonly FBeautyDbContext _context;

        public TimelineController(ILogger<SpaController> logger, FBeautyDbContext context)
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.listSpa = _context.SpaTreatmentPlans.ToList();
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);

            var timelineEntries = _context.SPTUsers
                .Where(u => u.UserId == userId)
                .OrderByDescending(u => u.StartDate)
                .ToList();

            return View(timelineEntries);
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using NuGet.Packaging;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using System.ComponentModel.Design;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class SpaController : Controller
    {
        private readonly ILogger<SpaController> _logger;
        private readonly FBeautyDbContext _context;
        public SpaController(ILogger<SpaController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        public IActionResult Index()
        {
            var stringName = HttpContext.Session.GetString("Searchs");
            
            if (stringName != null)
            {
                ViewBag.listSpa = _context.SpaTreatmentPlans
               .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName))
               .ToList();
                return View();
            }
            ViewBag.listSpa = _context.SpaTreatmentPlans.ToList();
            return View();
        }
        public IActionResult VendorMenu()
        {
           
            ViewBag.listSpa = _context.SpaTreatmentPlans.ToList();
            return View(); 
        }
        //public IActionResult Edit(Guid id)
        //{
        //    // Lấy thông tin SpaTreatmentPlan từ cơ sở dữ liệu theo id
        //    var editStps = _context.SpaTreatmentPlans.Find(id);

        //    if (editStps == null)
        //    {
        //        return NotFound(); // Hoặc xử lý tùy theo logic của bạn khi không tìm thấy
        //    }

        //    return View(editStps);
        //}
        [HttpPost]
        public IActionResult Search(string name)
        {
            if (name == null)
            {
                HttpContext.Session.SetString("Searchs", "");
                return RedirectToAction("Index", "Spa");
            }
            HttpContext.Session.SetString("Searchs", name);
            return RedirectToAction("Index", "Spa");
        }
        public IActionResult Delete(Guid id)
        {
            var itemToDelete = _context.SpaTreatmentPlans.Find(id);
            if(itemToDelete != null)
            {
                itemToDelete.Status = !itemToDelete.Status;
                _context.SaveChanges();
            }
            return RedirectToAction("VendorMenu");
        }
        
        public IActionResult EditSpa(string id)
        {
            string role = CheckRole();
            if (role == "VENDOR")
            {
                var stps = _context.SpaTreatmentPlans.FirstOrDefault(u => u.Id.ToString() == id);
                var company = _context.Companies.ToList();
                if (stps == null)
                {
                    return Conflict();
                }
                ViewBag.company = company;
                ViewBag.stps = stps;
                return View();
            }
            return Conflict();
        }
        [HttpPost]
        public ActionResult Edit(SpaTreatmentPlan model)
        {
            Guid id = model.Id;
            var stps = _context.SpaTreatmentPlans.Find(id);
                if (stps != null)
                {
                    // Cập nhật thuộc tính của existingProduct dựa trên dữ liệu đầu vào
                    stps.Name = model.Name;
                    stps.ShortDescription = model.ShortDescription;
                    stps.Price = model.Price;
                    stps.Discount = model.Discount;
                    stps.Duration = model.Duration;
                    stps.ReleasedDate = model.ReleasedDate;
                    stps.FullDescription = model.FullDescription;
                    //stps.Status = model.Status;
                    // Lưu thay đổi vào DbContext
                    _context.SaveChanges();
                }
            //}

            //if (stps != null)
            //{
            //    stps.Name = model.Name;
            //    stps.ShortDescription = model.ShortDescription;
            //    stps.Price = model.Price;
            //    stps.Discount = model.Discount;
            //    stps.Duration = model.Duration;
            //    stps.ReleasedDate= model.ReleasedDate;
            //    stps.FullDescription= model.FullDescription;
            //    //.Status = model.Status;
            //    _context.SaveChanges();
            //}
            return RedirectToAction("VendorMenu", "Spa");
        }

        public IActionResult Add()
            {
            var company = _context.Companies.ToList();
            ViewBag.company = company;
            return View();
        }
        public IActionResult AddSpa(SpaTreatmentPlan stps)
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);          
                SpaTreatmentPlan stps1 = new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = stps.Name,
                    ShortDescription = stps.ShortDescription,
                    FullDescription = stps.FullDescription,
                    Price = stps.Price,
                    Duration = stps.Duration,
                    Images = " ",                   
                    Discount = stps.Discount,
                    Status = stps.Status,
                    CompanyId = stps.CompanyId,
                };
            _context.SpaTreatmentPlans.Add(stps1);
            SPTUser sptUser = new SPTUser
            {
                STPId = stps1.Id,
                UserId = Guid.Parse(rawUserId)
            };
            _context.SPTUsers.Add(sptUser);
            _context.SaveChanges();
            return RedirectToAction("VendorMenu", "Spa");
        }

        public ActionResult Book(string? id)
        {
            ViewBag.listSpa = _context.SpaTreatmentPlans.ToList();
            try
            {
                string uid = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Guid userId = Guid.Parse(uid);
                Guid productId = Guid.Parse(id);
                var stpUser = new SPTUser
                {
                    UserId = userId,
                    STPId = productId,
                    StartDate = DateTime.Now,
                };
                _context.SPTUsers.Add(stpUser);
                _context.SaveChanges();

                ViewBag.Message = "Đặt dịch vụ thành công!";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Bạn đã đặt dịch vụ này!";
            }
            return View("Index");
        }
        public string CheckRole()
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            var roleid = _context.UserRoles.FirstOrDefault(u => u.UserId == userId);
            var role = _context.Roles.FirstOrDefault(u => u.Id == roleid.RoleId);
            return role.NormalizedName;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.Product;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Win32;

namespace SWP_GROUP4.Controllers
{
    public class VendorController : Controller
    {
        private readonly ILogger<VendorController> _logger;
        private readonly FBeautyDbContext _context;
        public VendorController(ILogger<VendorController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        public IActionResult Index()
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (rawUserIds == null)
            {
                return Redirect($"Authentication/Login");
            }
            string role = CheckRole();
            if (role == "VENDOR")
            {
                string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Guid userId = Guid.Parse(rawUserId);
                var productid = _context.ProductUsers.Where(u => u.UserId == userId).ToList();
                var listproduct = new List<Product>();
                foreach (var item in productid)
                {
                    var product = _context.Products.Where(u => u.Id == item.ProductId).ToList();
                    listproduct.AddRange(product);
                }
                ViewBag.listProduct = listproduct;
                ViewBag.user = userId.ToString();
                return View();
            }
            return NotFound();
        }
        public IActionResult EditProduct(string id)
        {
            string role = CheckRole();
            if (role == "VENDOR")
            {
                var product = _context.Products.FirstOrDefault(u => u.Id.ToString() == id);
                var company = _context.Companies.ToList();
                if (product == null)
                {
                    return Conflict();
                }
                var categori = _context.Categories.ToList();
                ViewBag.company = company;
                ViewBag.categori = categori;
                ViewBag.product = product;
                return View();
            }
            return Conflict();
        }
        [HttpPost]
        public ActionResult Edit(Product model, IFormFile productImage)
        {
            Guid id = model.Id;
            var product = _context.Products.FirstOrDefault(u => u.Id == model.Id);
            if (productImage != null && productImage.Length > 0)
            {
                var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/product");
                //var existingImagePath = Path.Combine(imageFolderPath, product.Images);
                System.IO.File.Delete(Directory.GetCurrentDirectory() + "/wwwroot/" + product.Images);
                // Lưu hình ảnh mới
                string fileNameImage = product.Images.Substring(product.Images.LastIndexOf('/') + 1);
                var fileName = Path.GetFileName(productImage.FileName);
                var filePath = Path.Combine(imageFolderPath, fileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    productImage.CopyTo(stream);
                }
                // Lưu đường dẫn tới hình ảnh trong model
                model.Images = "/img/product/" + fileName;
                if (product != null)
                {
                    // Cập nhật thuộc tính của existingProduct dựa trên dữ liệu đầu vào
                    product.Name = model.Name;
                    product.Price = model.Price;
                    product.Images = "/img/product/" + fileName;
                    product.IsCombo = model.IsCombo;
                    product.Quantity = model.Quantity;
                    product.Discount = model.Discount;
                    product.Status = model.Status;
                    product.CategoryId = model.CategoryId;
                    // Lưu thay đổi vào DbContext
                    _context.SaveChanges();
                }
            }

            if (product != null)
            {
                product.Name = model.Name;
                product.Price = model.Price;
                product.IsCombo = model.IsCombo;
                product.Quantity = model.Quantity;
                product.Discount = model.Discount;
                product.Status = model.Status;
                product.CategoryId = model.CategoryId;
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Vendor");
        }

        public IActionResult AddListDiscount()
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (rawUserIds == null)
            {
                return Redirect($"Authentication/Login");
            }
            string role = CheckRole();
            if (role == "VENDOR")
            {
                //var listproduct = _context.ProductUsers.Where(u => u.UserId.ToString() == rawUserIds).ToList();
                //List<string> cateid = new List<string>();
                //List<ProductCategory> listcate = new List<ProductCategory>();
                //foreach (var item in listproduct)
                //{
                //    listcate = _context.ProductCategories.Where(u => u.ProductId == item.ProductId).ToList();
                //}
                //foreach (var item in listcate)
                //{
                //    cateid.Add(item.CategoryId.ToString());
                //}
                var categori = _context.Categories.ToList();
                ViewBag.categori = categori;
                return View();
            }
            else
            {
                return Redirect($"Authentication/Login");
            }
        }
        public IActionResult DeleteProduct(string id)
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            //var product = _context.Products.FirstOrDefault(u => u.Id.ToString() == id);
            var productuser = _context.ProductUsers.Where(u => u.ProductId.ToString()== id).ToList();
            if (productuser != null)
            {
                _context.ProductUsers.RemoveRange(productuser);
                _context.SaveChanges();
                return RedirectToAction("Index", "Vendor");
            }
            else
            {
                return Redirect($"Authentication/Login");
            }
        }
        public IActionResult Add()
        {
            var company = _context.Companies.ToList();
            var categori = _context.Categories.ToList();
            ViewBag.company = company;
            ViewBag.categori = categori;
            return View();
        }
            public IActionResult AddProduct(Product product, IFormFile productImage)
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (productImage != null && productImage.Length > 0)
            {
                var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/product");
                //var existingImagePath = Path.Combine(imageFolderPath, product.Images);
                //System.IO.File.Delete(Directory.GetCurrentDirectory() + "/wwwroot/" + product.Images);
                // Lưu hình ảnh mới
                //string fileNameImage = product.Images.Substring(product.Images.LastIndexOf('/') + 1);
                var fileName = Path.GetFileName(productImage.FileName);
                var filePath = Path.Combine(imageFolderPath, fileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    productImage.CopyTo(stream);
                }
                // Lưu đường dẫn tới hình ảnh trong model
                //model.Images = "/img/product/" + fileName;
                Product product1 = new Product
                {
                    Id = Guid.NewGuid(),
                    Name = product.Name,
                    ShortDescription = product.ShortDescription,
                    FullDescription = product.FullDescription,
                    Price = product.Price,
                    Images = "/img/product/" + fileName,
                    IsCombo = product.IsCombo,
                    Quantity = product.Quantity,
                    Discount= product.Discount,
                    Status= product.Status,
                    CompanyId = product.CompanyId,
                    CategoryId = product.CategoryId,
                    ReleasedDate = DateTime.Now
                 };
                _context.Products.Add(product1);
                ProductUser productUser = new ProductUser
                {
                    ProductId = product1.Id,
                    UserId = Guid.Parse(rawUserId)
                    
                };
                _context.ProductUsers.Add(productUser);
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Vendor");
        }

        public string CheckRole()
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            var roleid = _context.UserRoles.FirstOrDefault(u => u.UserId == userId);
            var role = _context.Roles.FirstOrDefault(u => u.Id == roleid.RoleId);
            return role.NormalizedName;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace SWP_GROUP4.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ILogger<CompanyController> _logger;
        private readonly FBeautyDbContext _context;
        public CompanyController(ILogger<CompanyController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        //static string namess = "vendor2";
        public IActionResult Index()
        {
            var user = CheckUser();
            if (user == null)
            {
                return Redirect($"Authentication/Login");
            }
            var company = _context.Companies.FirstOrDefault(u=>u.UserId == user.Id);
            if (company == null)
            {
                ViewBag.UserId = user.Id.ToString();
                return RedirectToAction("AddCompanieForm", "Technologi");
            }
            var tech = _context.technologies.FirstOrDefault(u => u.Id == company.TechnologyId);
            ViewBag.Company = company;
            ViewBag.Tech = tech;
            return View();
        }
        public IActionResult EditCompany(Company companie, IFormFile Image)
        {
            var user = CheckUser();
            if (IsImageValid(Image) == false)
            {
                var companys = _context.Companies.FirstOrDefault(u => u.UserId == user.Id);
                var techs = _context.technologies.FirstOrDefault(u => u.Id == companys.TechnologyId);
                ViewBag.Company = companys;
                ViewBag.Tech = techs;
                ViewData["Error"] = "File Ảnh Không Đúng Định Dạng Hoặc Kích Thước >5MB Và Không Chứa Kí Tự Đặc Biệt";
                return View("Index");
            }
            var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/companie");
            var imagePath = Path.GetFileName(Image.FileName);
            string fileName = Path.GetFileNameWithoutExtension(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string randomPart = DateTime.Now.Second.ToString(); // Lấy đơn vị giây
            string newFileName = fileName + "_" + randomPart + fileExtension;
            var filePath = Path.Combine(imageFolderPath, newFileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                Image.CopyTo(stream);
            }
            var editCompany = _context.Companies.FirstOrDefault(u => u.Id == companie.Id);
            if(editCompany!= null)
            {
                editCompany.Name = companie.Name; 
                editCompany.ShortDescription    = companie.ShortDescription;
                editCompany.FullDescription= companie.FullDescription;
                editCompany.Image = "/img/companie/" + newFileName;
                editCompany.Location = companie.Location;
                _context.SaveChanges();
            };
            var company = _context.Companies.FirstOrDefault(u => u.UserId == user.Id);
            var tech = _context.technologies.FirstOrDefault(u => u.Id == company.TechnologyId);
            ViewBag.Company = company;
            ViewBag.Tech = tech;
            return View("Index");
        }
        public ApplicationUser CheckUser()
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (rawUserIds == null)
            {
                return null;
            }
            return _context.Users.FirstOrDefault(u => u.Id.ToString() == rawUserIds);
        }
        private static readonly string[] AllowedExtensions = { ".jpg", ".jpeg", ".png", ".gif" };
        private const int MaxFileSize = 5 * 1024 * 1024; // 5 MB
        public bool IsImageValid(IFormFile file)
        {
            // Check if the file is not null
            if (file == null)
                return false;

            // Check file extension
            var extension = Path.GetExtension(file.FileName).ToLowerInvariant();
            if (!AllowedExtensions.Contains(extension))
                return false;

            // Check file size
            if (file.Length > MaxFileSize)
                return false;

            // Check if file name contains Vietnamese characters
            if (ContainsVietnameseCharacters(file.FileName))
                return false;

            return true;
        }

        private bool ContainsVietnameseCharacters(string input)
        {
            // Pattern to match Vietnamese characters
            var vietnamesePattern = new Regex(@"[^\x00-\x7F]+");
            return vietnamesePattern.IsMatch(input);
        }
    }
}

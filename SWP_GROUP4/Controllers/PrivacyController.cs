﻿using Microsoft.AspNetCore.Mvc;
using SWP_GROUP4.DbContexts;

namespace SWP_GROUP4.Controllers
{
    public class PrivacyController : Controller
    {
        private readonly ILogger<PrivacyController> _logger;
        private readonly FBeautyDbContext _context;
        public PrivacyController(ILogger<PrivacyController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        public IActionResult Index()
        {
            var lisstpri = _context.privacies.ToList(); 
            ViewBag.privacy = _context.privacies.ToList();
            return View();
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;

namespace SWP_GROUP4.Controllers
{
    public class SpaDetailController : Controller
    {
        private readonly ILogger<SpaDetailController> _logger;
        private readonly FBeautyDbContext _context;
        public SpaDetailController(ILogger<SpaDetailController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        public IActionResult Index(string id)
        {
            var stps = _context.SpaTreatmentPlans.FirstOrDefault(u => u.Id.ToString() == id);
            ViewBag.stps = stps;
            return View();
        }
    }
}
﻿    using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.Product;
using SWP_GROUP4.ViewModels.User;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class PaymentController : Controller
    {
        private readonly FBeautyDbContext _context;
        List<ProductViewModel> Products = new();

        public PaymentController(FBeautyDbContext context)
        {
            _context = context;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> PaymentRequest()
        {
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(id);
            ApplicationUser rawUser = await _context.Users.FindAsync(userId);
            List<ProductUser> pus = await _context.ProductUsers.Where(pu => pu.UserId == userId).ToListAsync();
            Order o = new Order {
                Id = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                PaymentStatus = false,
                UserId = userId
            };
            List<ProductOrder> pos = new();
            // for each item in cart, add to order
            foreach(var item in pus)
            {
                ProductOrder p = new ProductOrder
                {
                    ProductId = item.ProductId,
                    OrderId = o.Id,
                    Quantity = item.Quantity
                };
                pos.Add(p);
            }
            // add order
            await _context.Orders.AddAsync(o);
            // add order item
            await _context.ProductOrders.AddRangeAsync(pos);
            // delete product in cart
            _context.ProductUsers.RemoveRange(pus);
            await _context.SaveChangesAsync();
            ConfirmPaymentViewModel cf = new ConfirmPaymentViewModel
            {
                FirstName = rawUser.FirstName,
                LastName = rawUser.LastName,
                BirthDate = rawUser.BirthDate,
                About = rawUser.About,
                UserName = rawUser.UserName,
                Email = rawUser.Email,
                Address = rawUser.Address,
                ShipAddress = rawUser.ShipAddress,
                PhoneNumber = rawUser.PhoneNumber,
                OrderId = o.Id
            };
            return View(cf);
        }

        [Authorize]
        public async Task<IActionResult> PaymentResponse()
        {
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(id);
            ApplicationUser rawUser = await _context.Users.FindAsync(userId);
            List<ProductUser> ProductUsers = _context.ProductUsers.Where(pu => pu.UserId == userId).ToList();
            foreach (var item in ProductUsers)
            {
                Product p = _context.Products.FirstOrDefault(p => p.Id == item.ProductId);

                ProductViewModel pvm = new ProductViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    ShortDescription = p.ShortDescription,
                    Images = p.Images,
                    Price = p.Price,
                    Quantity = item.Quantity,
                    Discount = p.Discount
                };
                Products.Add(pvm);
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUserInfo(UserViewModel updatedUser)
        {
            if (ModelState.IsValid)
            {
                // Lấy thông tin người dùng từ cơ sở dữ liệu
                string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
                Guid userId = Guid.Parse(id);
                ApplicationUser rawUser = await _context.Users.FindAsync(userId);
                
                // Cập nhật thông tin người dùng với dữ liệu mới
                rawUser.FirstName = updatedUser.FirstName;
                rawUser.LastName = updatedUser.LastName;
                rawUser.Email = updatedUser.Email;
                rawUser.PhoneNumber = updatedUser.PhoneNumber;
                rawUser.Address = updatedUser.Address;
                rawUser.ShipAddress = updatedUser.ShipAddress;

                _context.Update(rawUser);
                await _context.SaveChangesAsync();
                
                return RedirectToAction("PaymentRequest");
            }

            // Nếu ModelState không hợp lệ, trả về view với dữ liệu người dùng để hiển thị lỗi
            return View("PaymentRequest", updatedUser);
        }

    }
}
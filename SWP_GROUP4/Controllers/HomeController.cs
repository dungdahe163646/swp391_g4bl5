﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Security.Claims;
using System.Security.Policy;
using System.Xml.Linq;

namespace SWP_GROUP4.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly FBeautyDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public HomeController(ILogger<HomeController> logger, FBeautyDbContext context, UserManager<ApplicationUser> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {

            var stringName = HttpContext.Session.GetString("Search");
            var LTH = HttpContext.Session.GetString("LTH");
            var HTL = HttpContext.Session.GetString("HTL");
            var Combo = HttpContext.Session.GetString("Combo");
            var rawCateId = HttpContext.Session.GetString("CateId");
            var discountedProducts = _context.Products
            .Where(u => u.IsCombo == true && u.Discount > 0).Where(u => u.Status == true)
            .OrderByDescending(u => u.Discount)
            .Take(5)
            .ToList();
            var productBaner = _context.Products
            .Where(u => u.IsCombo == true)
            .OrderByDescending(u=>u.Discount)
            .Take(3)
            .ToList();
            if (rawCateId != null)
            {
                var cateId = _context.Categories.Find(Guid.Parse(rawCateId)).Id;
                var listproduct = new List<Product>();
                //foreach (var item in listid)
                //{
                    var product = _context.Products.Where(p => p.CategoryId == cateId).ToList();
                    listproduct.AddRange(product);
                //}
                if (stringName != null)
                {
                    if (Combo != null)
                    {
                        if (LTH != null)
                        {
                            ViewBag.listProduct =
                            listproduct
                            .Where(u => (string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)) &&
                            (string.IsNullOrEmpty(Combo) || u.IsCombo == true)).Where(u => u.Status == true)
                            .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                            .ToList();
                            ViewBag.listCategori = _context.Categories.ToList();
                            ViewBag.productBaner = productBaner;
                            ViewBag.discountedProducts = discountedProducts;
                            ViewBag.privacy = _context.privacies.ToList();
                            return View();
                        }
                        if (HTL != null)
                        {
                            ViewBag.listProduct = listproduct
                           .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                           .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                           .ToList();
                            ViewBag.listCategori = _context.Categories.ToList();
                            ViewBag.productBaner = productBaner;
                            ViewBag.discountedProducts = discountedProducts;
                            ViewBag.privacy = _context.privacies.ToList();
                            return View();
                        }
                        ViewBag.listProduct = listproduct
                           .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                           .ToList();
                        ViewBag.discountedProducts = discountedProducts;
                        ViewBag.productBaner = productBaner;
                        ViewBag.listCategori = _context.Categories.ToList();
                        ViewBag.privacy = _context.privacies.ToList();
                        return View();
                    }
                    if (LTH != null)
                    {
                        ViewBag.listProduct = listproduct
                       .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                       .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                       .ToList();
                        ViewBag.discountedProducts = discountedProducts;
                        ViewBag.productBaner = productBaner;
                        ViewBag.listCategori = _context.Categories.ToList();
                        ViewBag.privacy = _context.privacies.ToList();
                        return View();
                    }
                    if (HTL != null)
                    {
                        ViewBag.listProduct = listproduct
                       .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                       .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                       .ToList();
                        ViewBag.discountedProducts = discountedProducts;
                        ViewBag.productBaner = productBaner;
                        ViewBag.listCategori = _context.Categories.ToList();
                        ViewBag.privacy = _context.privacies.ToList();
                        return View();
                    }
                    ViewBag.listProduct = listproduct
                   .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => u.Status == true)
                   .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                if (Combo != null)
                {
                    if (LTH != null)
                    {
                        ViewBag.listProduct = listproduct
                       .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                       .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                       .ToList();
                        ViewBag.discountedProducts = discountedProducts;
                        ViewBag.productBaner = productBaner;
                        ViewBag.listCategori = _context.Categories.ToList();
                        ViewBag.privacy = _context.privacies.ToList();
                        return View();
                    }
                    if (HTL != null)
                    {
                        ViewBag.listProduct = listproduct
                       .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                       .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                       .ToList();
                        ViewBag.discountedProducts = discountedProducts;
                        ViewBag.productBaner = productBaner;
                        ViewBag.listCategori = _context.Categories.ToList();
                        ViewBag.privacy = _context.privacies.ToList();
                        return View();
                    }
                    ViewBag.listProduct = listproduct
                      .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                      .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                if (LTH != null)
                {
                    ViewBag.listProduct = listproduct
                   .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                   .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                   .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                if (HTL != null)
                {
                    ViewBag.listProduct = listproduct
                   .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                   .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                   .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                ViewBag.discountedProducts = discountedProducts;
                ViewBag.listCategori = _context.Categories.ToList();
                ViewBag.productBaner = productBaner;
                ViewBag.listProduct = listproduct.ToList();
                ViewBag.privacy = _context.privacies.ToList();
                return View();
            }
            if (stringName != null)
            {
                if (Combo != null)
                {
                    if (LTH != null)
                    {
                        _context.Products
                        .Where(u => (string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)) &&
                        (string.IsNullOrEmpty(Combo) || u.IsCombo == true)).Where(u => u.Status == true)
                        .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                        .ToList();
                        ViewBag.discountedProducts = discountedProducts;
                        ViewBag.productBaner = productBaner;
                        ViewBag.listCategori = _context.Categories.ToList();
                        ViewBag.privacy = _context.privacies.ToList();
                        return View();
                    }
                    if (HTL != null)
                    {
                        ViewBag.listProduct = _context.Products
                       .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                       .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                       .ToList();
                        ViewBag.discountedProducts = discountedProducts;
                        ViewBag.productBaner = productBaner;
                        ViewBag.listCategori = _context.Categories.ToList();
                        ViewBag.privacy = _context.privacies.ToList();
                        return View();
                    }
                    ViewBag.listProduct = _context.Products
                       .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                       .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                if (LTH != null)
                {
                    ViewBag.listProduct = _context.Products
                   .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                   .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                   .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                if (HTL != null)
                {
                    ViewBag.listProduct = _context.Products
                   .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                   .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                   .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                ViewBag.listProduct = _context.Products
               .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => u.Status == true)
               .ToList();
                ViewBag.discountedProducts = discountedProducts;
                ViewBag.productBaner = productBaner;
                ViewBag.listCategori = _context.Categories.ToList();
                ViewBag.privacy = _context.privacies.ToList();
                return View();
            }
            if (Combo != null)
            {
                if (LTH != null)
                {
                    ViewBag.listProduct = _context.Products.Where(u => u.Status == true)
                   .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                   .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                   .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                if (HTL != null)
                {
                    ViewBag.listProduct = _context.Products
                   .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                   .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
                   .ToList();
                    ViewBag.discountedProducts = discountedProducts;
                    ViewBag.productBaner = productBaner;
                    ViewBag.listCategori = _context.Categories.ToList();
                    ViewBag.privacy = _context.privacies.ToList();
                    return View();
                }
                ViewBag.listProduct = _context.Products
                  .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
                  .ToList();
                ViewBag.discountedProducts = discountedProducts;
                ViewBag.productBaner = productBaner;
                ViewBag.listCategori = _context.Categories.ToList();
                ViewBag.privacy = _context.privacies.ToList();
                return View();
            }
            if (LTH != null)
            {
                ViewBag.listProduct = _context.Products
               .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
               .OrderBy(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
               .ToList();
                ViewBag.discountedProducts = discountedProducts;
                ViewBag.productBaner = productBaner;
                ViewBag.listCategori = _context.Categories.ToList();
                ViewBag.privacy = _context.privacies.ToList();
                return View();
            }
            if (HTL != null)
            {
                ViewBag.listProduct = _context.Products
               .Where(u => string.IsNullOrEmpty(stringName) || u.Name.Contains(stringName)).Where(u => string.IsNullOrEmpty(Combo) || u.IsCombo == true).Where(u => u.Status == true)
               .OrderByDescending(u => u.Price * (100 - (u.Discount ?? 0)) / 100)
               .ToList();
                ViewBag.discountedProducts = discountedProducts;
                ViewBag.productBaner = productBaner;
                ViewBag.privacy = _context.privacies.ToList();
                ViewBag.listCategori = _context.Categories.ToList();
                return View();
            }
            ViewBag.privacy = _context.privacies.ToList();
            ViewBag.discountedProducts = discountedProducts;
            ViewBag.productBaner = productBaner;
            ViewBag.listCategori = _context.Categories.ToList();
            ViewBag.listProduct = _context.Products.Where(u=>u.Status== true).ToList();
            return View();
        }
        public IActionResult LTH()
        {
            HttpContext.Session.SetString("LTH", "LTH");
            HttpContext.Session.Remove("HTL");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult NoLTH()
        {
            HttpContext.Session.Remove("LTH");
            HttpContext.Session.Remove("HTL");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult HTL()
        {
            HttpContext.Session.SetString("HTL", "HTL");
            HttpContext.Session.Remove("LTH");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult NoHTL()
        {
            HttpContext.Session.Remove("LTH");
            HttpContext.Session.Remove("HTL");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult Combo()
        {
            HttpContext.Session.SetString("Combo", "Combo");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult NoCombo()
        {
            HttpContext.Session.Remove("Combo");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult ListAll()
        {
            HttpContext.Session.Remove("LTH");
            HttpContext.Session.Remove("HTL");
            HttpContext.Session.Remove("Combo");
            HttpContext.Session.Remove("Search");
            HttpContext.Session.Remove("CateId");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult GetProductByCate(string cateId)
        {
            if (cateId != null)
            {
                HttpContext.Session.SetString("CateId", cateId);
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public IActionResult Search(string name)
        {
            if (name == null)
            {
                HttpContext.Session.SetString("Search", "");
                return RedirectToAction("Index", "Home");
            }
            HttpContext.Session.SetString("Search", name);
            return RedirectToAction("Index", "Home");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
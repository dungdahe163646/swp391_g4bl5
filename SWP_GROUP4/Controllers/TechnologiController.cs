﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using System.Security.Claims;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using PagedList;
using System.Linq;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Text.RegularExpressions;
using SWP_GROUP4.ViewModels.Company;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Authorization;

namespace SWP_GROUP4.Controllers
{
    public class TechnologiController : Controller
    {
        private readonly ILogger<TechnologiController> _logger;
        private readonly FBeautyDbContext _context;
        public TechnologiController(ILogger<TechnologiController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        //static string namess = "vendor2";
        public IActionResult Index(int index)
        {

            if (index == 0 || index == null)
            {
                index = 1;
            }
            int totalPage = 0;
            int totalTechInPage = 2;
            var user = CheckUser();
            var compani = _context.Companies.FirstOrDefault(u => u.UserId.ToString() == user.Id.ToString());
            //var test = GetAllTechnology();
            if (compani == null)
            {
                var techss = _context.technologies.ToList();
                totalPage = techss.Count / 2;
                if (techss.Count % 2 != 0)
                {
                    totalPage += 1;
                }
                ViewBag.totalPage = totalPage;
                ViewBag.techs = techss.Skip(totalTechInPage * (index - 1)).Take(totalTechInPage).ToList();
                return View();
            }
            var techDefault = _context.technologies.FirstOrDefault(u => u.Id == compani.TechnologyId);
            var companitech = _context.companyTechnologies.Where(u => u.CompanyId == compani.Id).ToList();
            if (companitech == null || techDefault == null)
            {
                var techss = _context.technologies.ToList();
                totalPage = techss.Count / 2;
                if (techss.Count % 2 != 0)
                {
                    totalPage += 1;
                }
                ViewBag.totalPage = totalPage;
                ViewBag.techs = techss.Skip(totalTechInPage * (index - 1)).Take(totalTechInPage).ToList();
                return View();
            }
            var companitechs = _context.companyTechnologies.Where(u => u.CompanyId == compani.Id && u.TechnologyId != techDefault.Id).ToList();
            var checkstatus = _context.companyTechnologies.Where(u => u.Status == 2).ToList();
            List<Technology> tech = new List<Technology>();
            foreach (var item in companitechs)
            {
                var techof = _context.technologies.Where(u => u.Id == item.TechnologyId && item.Status == 1).ToList();
                tech.AddRange(techof);
            }
            List<Technology> techs = _context.technologies.ToList();
            string searchString = HttpContext.Session.GetString("searchString");
            if (!string.IsNullOrEmpty(searchString))
            {
                techs = techs.Where(tech => tech.Name.Contains(searchString)).ToList();
            }
            foreach (var item in companitech)
            {
                techs = techs.Where(u => u.Id != item.TechnologyId).ToList();
            }
            //foreach (var item in checkstatus)
            //{
            //    tech = techs.Where(u => u.Id != item.TechnologyId).ToList();
            //}
            totalPage = techs.Count / 2;
            if (techs.Count % 2 != 0)
            {
                totalPage += 1;
            }
            techs = techs.Skip(totalTechInPage * (index - 1)).Take(totalTechInPage).ToList();
            ViewBag.totalPage = totalPage;
            ViewBag.techs = techs;
            ViewBag.tech = tech;
            ViewBag.techDefault = techDefault;
            return View();
            //return View(techs.ToPagedList(pageNumber, pageSize));
        }
        public IActionResult Search(string searchString)
        {
            HttpContext.Session.SetString("searchString", searchString);
            return RedirectToAction("Index", "Technologi");
        }
        public IActionResult Edit(string id)
        {
            var tech = _context.technologies.FirstOrDefault(u => u.Id.ToString() == id);
            if (tech != null)
            {
                ViewBag.tech = tech;
                return View();
            }
            return Redirect($"Authentication/Login");
        }
        public IActionResult EditTech(Technology tech, IFormFile Image)
        {
            if (IsImageValid(Image) == false)
            {
                var techs = _context.technologies.FirstOrDefault(u => u.Id == tech.Id);
                ViewBag.tech = techs;
                ViewData["Error"] = "File Ảnh Không Đúng Định Dạng Hoặc Kích Thước >5MB Và Không Chứa Kí Tự Đặc Biệt";
                return View("Edit");
            }
            var checkTech = _context.technologies.FirstOrDefault(u => u.Id == tech.Id);
            var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/tech");
            var imagePath = Path.GetFileName(Image.FileName);
            string fileName = Path.GetFileNameWithoutExtension(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string randomPart = DateTime.Now.Second.ToString(); // Lấy đơn vị giây

            string newFileName = fileName + "_" + randomPart + fileExtension;
            var filePath = Path.Combine(imageFolderPath, newFileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                Image.CopyTo(stream);
            }
            if (checkTech != null)
            {
                checkTech.Name = tech.Name;
                checkTech.ShortDescription = tech.ShortDescription;
                checkTech.FullDescription = tech.FullDescription;
                checkTech.Image = "/img/tech/" + newFileName;
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Technologi");
        }
        public IActionResult Add()
        {
            var user = CheckUser();
            var compani = _context.Companies.FirstOrDefault(u => u.UserId.ToString() == user.Id.ToString());
            if (compani == null)
            {
                ViewBag.UserId = user.Id.ToString();
                return RedirectToAction("AddCompanieForm", "Technologi");
            }
            ViewBag.companieId = compani.Id.ToString();
            return View();
        }
        public IActionResult AddCompanieForm()
        {
            var user = CheckUser();
            ViewBag.userId = user.Id.ToString();
            return View();
        }
        public IActionResult AddCompanie(Company companie, IFormFile Image)
        {
            if (IsImageValid(Image) == false)
            {
                var user = CheckUser();
                ViewBag.userId = user.Id.ToString();
                ViewData["Error"] = "File Ảnh Không Đúng Định Dạng Hoặc Kích Thước >5MB Và Không Chứa Kí Tự Đặc Biệt";
                return View("AddCompanieForm");
            }
            var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/companie");
            var imagePath = Path.GetFileName(Image.FileName);
            string fileName = Path.GetFileNameWithoutExtension(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string randomPart = DateTime.Now.Second.ToString(); // Lấy đơn vị giây

            string newFileName = fileName + "_" + randomPart + fileExtension;
            var filePath = Path.Combine(imageFolderPath, newFileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                Image.CopyTo(stream);
            }
            Company addComanie = new Company
            {
                Id = Guid.NewGuid(),
                Name = companie.Name,
                ShortDescription = companie.ShortDescription,
                FullDescription = companie.FullDescription,
                Image = "/img/companie/" + newFileName,
                Location = companie.Location,
                Status = companie.Status,
                UserId = companie.UserId
            };
            _context.Companies.Add(addComanie);
            _context.SaveChanges();
            return RedirectToAction("checkCompanie", "Technologi");
        }
        public IActionResult checkCompanie()
        {
            var user = CheckUser();
            var compani = _context.Companies.FirstOrDefault(u => u.UserId.ToString() == user.Id.ToString());
            //TempData["CompanieId"] = compani.Id.ToString();
            return RedirectToAction("Add", "Technologi");
        }
        public IActionResult AddTech(Technology tech, IFormFile Image)
        {
            var user = CheckUser();
            var compani = _context.Companies.FirstOrDefault(u => u.UserId.ToString() == user.Id.ToString());
            if (compani == null)
            {
                ViewBag.UserId = user.Id.ToString();
                return RedirectToAction("AddCompanieForm", "Technologi");
            }
            if (IsImageValid(Image) == false)
            {
                ViewBag.companieId = compani.Id.ToString();
                ViewData["Error"] = "File Ảnh Không Đúng Định Dạng Hoặc Kích Thước >5MB Và Không Chứa Kí Tự Đặc Biệt";
                return View("Add");
            }
            var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/tech");
            var imagePath = Path.GetFileName(Image.FileName);
            string fileName = Path.GetFileNameWithoutExtension(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string randomPart = DateTime.Now.Second.ToString(); // Lấy đơn vị giây

            string newFileName = fileName + "_" + randomPart + fileExtension;
            var filePath = Path.Combine(imageFolderPath, newFileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                Image.CopyTo(stream);
            }
            Technology addTech = new Technology
            {
                Id = Guid.NewGuid(),
                Name = tech.Name,
                ShortDescription = tech.ShortDescription,
                FullDescription = tech.FullDescription,
                Image = "/img/tech/" + newFileName,
                CompanyId = compani.Id,
            };
            _context.technologies.Add(addTech);
            _context.SaveChanges();
            var getTech = _context.technologies.FirstOrDefault(u => u.CompanyId == compani.Id);
            CompanyTechnology companyTechnology = new CompanyTechnology
            {
                CompanyId = compani.Id,
                TechnologyId = getTech.Id,
                Status = 1
            };
            _context.companyTechnologies.Add(companyTechnology);
            if (compani != null)
            {
                compani.TechnologyId = getTech.Id;
                _context.SaveChanges();
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Technologi");
        }
        public IActionResult RequestTech(string id)
        {
            var user = CheckUser();
            var compani = _context.Companies.FirstOrDefault(u => u.UserId.ToString() == user.Id.ToString());
            if(compani == null) {
                ViewBag.UserId = user.Id.ToString();
                return RedirectToAction("AddCompanieForm", "Technologi");
            }
            CompanyTechnology companyTechnology = new CompanyTechnology
            {
                CompanyId = compani.Id,
                TechnologyId = Guid.Parse(id),
                Status = 2
            };
            _context.companyTechnologies.Add(companyTechnology);
            _context.SaveChanges();
            //var companieTech = _context.companyTechnologies.Where(u => u.CompanyId == compani.Id).Where(u => u.Status == 1).ToList();
            return RedirectToAction("Index", "Technologi");
        }
        public IActionResult ListRequestTech()
        {
            var user = CheckUser();
            List<Technology> tech = new List<Technology>();
            var compani = _context.Companies.FirstOrDefault(u => u.UserId.ToString() == user.Id.ToString());
            if (compani == null)
            {
                ViewBag.UserId = user.Id.ToString();
                return RedirectToAction("AddCompanieForm", "Technologi");
            }
            var checktech = _context.technologies.FirstOrDefault(u => u.Id == compani.TechnologyId);
            var companitech = _context.companyTechnologies.Include(u => u.Technology).Where(u => u.CompanyId == compani.Id && u.TechnologyId != checktech.Id).ToList();
            var viewModel = new CompanyTechnologyViewModel
            {
                Technologies = _context.companyTechnologies.Include(u => u.Technology).Where(u => u.CompanyId == compani.Id).Where(u => u.Status == 2).ToList()
            };
            List<TechRequest> techRequests = new List<TechRequest>();
            foreach (var item in companitech)
            {
                var techrequest = new TechRequest
                {
                    Name = item.Technology.Name,
                    ShortDescription = item.Technology.ShortDescription,
                    FullDescription = item.Technology.FullDescription,
                    Image = item.Technology.Image,
                    CompanyId = item.CompanyId,
                    TechnologyId = item.Technology.Id,
                    Status = item.Status
                };
                techRequests.Add(techrequest);
            }
            //foreach (var item in companitech)
            //{
            //    var techof = _context.companyTechnologies .Include(u => u.Technology).Where(u => u.Id == item.TechnologyId && item.Status == 2).ToList();
            //    tech.AddRange(techof);
            //}
            //var test = _context.companyTechnologies..Where(u=>u.Status ==2).ToList();
            ViewBag.ListTech = techRequests;
            return View();
        }
        public IActionResult ViewRequest()
        {
            var user = CheckUser();
            List<Technology> tech = new List<Technology>();
            var compani = _context.Companies.FirstOrDefault(u => u.UserId.ToString() == user.Id.ToString());
            if(compani == null) {
                ViewBag.UserId = user.Id.ToString();
                return RedirectToAction("AddCompanieForm", "Technologi");
            }
            var checktech = _context.technologies.FirstOrDefault(u => u.Id == compani.TechnologyId);
            var companitech = _context.companyTechnologies.Where(u => u.TechnologyId == checktech.Id).Where(u => u.Status == 2).ToList();
            foreach (var item in companitech)
            {
                var techof = _context.technologies.Where(u => u.Id == item.TechnologyId && item.Status == 2).ToList();
                tech.AddRange(techof);
            }
            List<Company> getcompanie = new List<Company>();
            List<ApplicationUser> users = new List<ApplicationUser>();
            foreach (var item in companitech)
            {
                getcompanie = _context.Companies.Where(u => u.Id == item.CompanyId).ToList();
            }
            foreach (var item in getcompanie)
            {
                users = _context.Users.Where(u => u.Id == item.UserId).ToList();
            }
            ViewBag.ListUser = users;
            ViewBag.ListTech = tech;
            ViewBag.ListCompanie = getcompanie;
            return View();
        }
        public IActionResult Acecp(string id, string comId)
        {
            var edit = _context.companyTechnologies.FirstOrDefault(u => u.TechnologyId.ToString() == id && u.CompanyId.ToString() == comId);
            if (edit != null)
            {
                edit.Status = 1;
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Technologi");
        }
        public IActionResult NotAcecp(string id, string comId)
        {
            var edit = _context.companyTechnologies.FirstOrDefault(u => u.TechnologyId.ToString() == id && u.CompanyId.ToString() == comId);
            if (edit != null)
            {
                edit.Status = 0;
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Technologi");
        }
        public IActionResult Delete(string id)
        {
            var deleteTech = _context.technologies.FirstOrDefault(u => u.Id.ToString() == id);
            var deleteCom = _context.Companies.FirstOrDefault(u=>u.TechnologyId.ToString() == id);
            var deleteComTech = _context.companyTechnologies.Where(u=>u.TechnologyId.ToString() == id).ToList();
            if (deleteTech != null || deleteCom !=null || deleteComTech != null)
            {
                _context.companyTechnologies.RemoveRange(deleteComTech);
                deleteCom.TechnologyId = null;
                _context.technologies.Remove(deleteTech);
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Technologi");
        } 
            public dynamic GetAllTechnology(string search, int? index)
        {
            int totalTechInPage = 2; 
            if(index== 0|| index == null)
            {
                index = 1;
            }
            var listTech = _context.Companies.Include(u => u.Technology).Include(u => u.User).ToList();
            return listTech;
        }

        public ApplicationUser CheckUser()
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (rawUserIds == null)
            {
                return null;
            }
            return _context.Users.FirstOrDefault(u => u.Id.ToString() == rawUserIds);
        }
        private static readonly string[] AllowedExtensions = { ".jpg", ".jpeg", ".png", ".gif" };
        private const int MaxFileSize = 5 * 1024 * 1024; // 5 MB
        public bool IsImageValid(IFormFile file)
        {
            // Check if the file is not null
            if (file == null)
                return false;

            // Check file extension
            var extension = Path.GetExtension(file.FileName).ToLowerInvariant();
            if (!AllowedExtensions.Contains(extension))
                return false;

            // Check file size
            if (file.Length > MaxFileSize)
                return false;

            // Check if file name contains Vietnamese characters
            if (ContainsVietnameseCharacters(file.FileName))
                return false;

            return true;
        }

        private bool ContainsVietnameseCharacters(string input)
        {
            // Pattern to match Vietnamese characters
            var vietnamesePattern = new Regex(@"[^\x00-\x7F]+");
            return vietnamesePattern.IsMatch(input);
        }
    }
}

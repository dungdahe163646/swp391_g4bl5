﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.User;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SWP_GROUP4.Controllers
{
    public class AdministratorController : Controller
    {
        private readonly FBeautyDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AdministratorController(
            FBeautyDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: /<controller>/
        public IActionResult Login()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return Redirect("/Home");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLoginViewModel userLogin)
        {

            var usersToDelete = _context.Users.Where(u => u.FirstName.Equals("")).ToList();
            if (usersToDelete.Count > 0)
            {
                _context.Users.RemoveRange(usersToDelete);
                _context.SaveChanges();
            }
            if (ModelState.IsValid)
            {
                var admin = _context.Administrators.FirstOrDefault(ad => ad.Email.ToLower().Equals(userLogin.Email.ToLower()));
                if (admin != null)
                {
                    byte[] salt = new byte[] { (byte)'a', (byte)'d', (byte)'m', (byte)'i', (byte)'n' };
                    var passwordHash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                        password: userLogin.Password,
                    salt: salt,
                    prf: KeyDerivationPrf.HMACSHA256,
                        iterationCount: 100000,
                        numBytesRequested: 256 / 8));
                    var result = _context.Administrators.Include(a => a.Position).FirstOrDefault(ad =>
                        ad.Email.ToLower().Equals(userLogin.Email.ToLower()) &&
                        ad.PasswordHash.Equals(passwordHash)
                    );
                    if (result != null)
                    {
                        var adminUser = new ApplicationUser
                        {
                            UserName = result.Email.Substring(0, result.Email.IndexOf('@')),
                            Email = result.Email,
                            FirstName = "",
                            LastName = "",
                            BirthDate = DateTime.MinValue,
                            About = "",
                            Address = "",
                            PhoneNumber = "",
                            Image = "",
                            Status = true
                        };
                        await _userManager.CreateAsync(adminUser, userLogin.Password);
                        if (result.Position.Name.Equals("Product Manager"))
                        {
                            await _userManager.AddToRoleAsync(adminUser, "PAdmin");
                            await _signInManager.SignOutAsync();
                            await _signInManager.SignInAsync(adminUser, isPersistent: false);
                            return Redirect("/PDashboard");
                        }
                        else if (result.Position.Name.Equals("User Manager"))
                        {
                            await _userManager.AddToRoleAsync(adminUser, "UAdmin");
                            await _signInManager.SignOutAsync();
                            await _signInManager.SignInAsync(adminUser, isPersistent: false);
                            return Redirect("/UDashboard");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("email", "Email doesn't exist in system");
                    return View(userLogin);
                }
                ModelState.AddModelError("password", "Wrong password!");
                return View(userLogin);
            }
            return View(userLogin);
        }

        [Authorize(Roles = "PAdmin,UAdmin")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(id);
            var result = await _userManager.DeleteAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("NullOrExpired");
            }
        }
    }
}


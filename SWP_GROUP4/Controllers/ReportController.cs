﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.Report;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SWP_GROUP4.Controllers
{
    public class ReportController : Controller
    {
        private readonly FBeautyDbContext _context;
        public ReportController(FBeautyDbContext context)
        {
            _context = context;
        }
        // GET: /<controller>/
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateReportViewModel createReportViewModel)
        {
            if (ModelState.IsValid)
            {
                Report report = new Report
                {
                    Id = Guid.NewGuid(),
                    Email = createReportViewModel.Email,
                    Phone = createReportViewModel.Phone,
                    Header = createReportViewModel.Header,
                    Content = createReportViewModel.Content,
                    ReportedDate = DateTime.Now
                };
                try
                {
                    await _context.Reports.AddAsync(report);
                    await _context.SaveChangesAsync();
                }
                catch (Exception)
                {
                    return View("/Error/SomethingWentWrong");
                }
                return View("CreateSuccessfully");
            }
            else
            {
                return View(createReportViewModel);
            }
        }
    }
}


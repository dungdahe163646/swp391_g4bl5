﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels;
using SWP_GROUP4.ViewModels.Product;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class ProductDetailController : Controller
    {
        private readonly ILogger<ProductDetailController> _logger;
        private readonly FBeautyDbContext _context;
        List<RateViewModel> RateView = new();
        public ProductDetailController(ILogger<ProductDetailController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        public async Task<IActionResult> Comment(string id, int rating, string header, string content)
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            ApplicationUser user = await _context.Users.FindAsync(userId);
            Guid productId = Guid.Parse(id);

            Rate newRate = new Rate
            {
                ProductId = productId,
                UserId = userId,
                Rating = rating,
                Header = header,
                Content = content,
                CreatedDate = DateTime.Now
            };

            _context.Rates.Add(newRate);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { id = id });
        }

        [Authorize]
        public async Task<IActionResult> Index(string id)
        {
            // Show productDetail by DatNT
            var product = _context.Products.Include(p => p.Category).Include(u => u.Company).FirstOrDefault(u => u.Id.ToString() == id);
            ViewBag.product = product;
            ViewBag.categori = product.Category;
            ViewBag.companie = _context.Companies.FirstOrDefault(u => u.Id == product.CompanyId);
            var discountedProducts = _context.Products
           .Where(u => u.IsCombo == true && u.Discount > 0)
           .OrderByDescending(u => u.Discount)
           .Take(5)
           .ToList();
            ViewBag.discountedProducts = discountedProducts;
            ViewBag.listCategori = _context.Categories.ToList();

            //Comment
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            ApplicationUser user = await _context.Users.FindAsync(userId);
            List<Rate> lrv = _context.Rates.Where(lr => lr.ProductId == product.Id).ToList();

            ViewBag.UserName = user.UserName;
            ViewBag.Rates = lrv;

            List<RateViewModel> RateVMs = await _context.Rates.Include(r => r.User)
                .Where(r => r.UserId == userId)
                .OrderByDescending(r => r.CreatedDate)
                .Select(rvm => new RateViewModel
                {
                    Rating = rvm.Rating,
                    Header = rvm.Header,
                    Content = rvm.Content,
                    UserImage = rvm.User.Image,
                    UserFullname = rvm.User.FirstName + " " + rvm.User.LastName,
                    CreatedDate = rvm.CreatedDate,
                })
                .ToListAsync();
            ViewBag.Rates = RateVMs;
            return View();
        }
    }
}

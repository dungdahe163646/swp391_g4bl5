﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class ScanController : Controller
    {
        private readonly ILogger<ScanController> _logger;
        private readonly FBeautyDbContext _context;
        public ScanController(ILogger<ScanController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [Authorize]
        public async Task<IActionResult> Index(string rawOrderId)
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserIds);
            Guid orderId = Guid.Parse(rawOrderId);
            Order o = await _context.Orders.Include(o => o.ProductOrders).ThenInclude(po => po.Product).FirstOrDefaultAsync(o => o.Id == orderId);
            //var order = _context.Orders.Where(u => u.UserId == userId).ToList();
            //List<ProductOrder> productOrders = new List<ProductOrder>();
            //foreach (var item in order)
            //{
            //    var productorder = _context.ProductOrders.Where(u => u.OrderId == item.Id).ToList();
            //    productOrders.AddRange(productorder);
            //}
            //List<Product> products = new List<Product>();
            //foreach (var item in productOrders)
            //{
            //    var product = _context.Products.Where(u => u.Id == item.ProductId).ToList();
            //    products.AddRange(product);
            //}
            //ViewBag.orders = order;
            ViewBag.Order = o;
            return View();
        }
        public IActionResult Canel(string id)
        {
            string rawUserIds = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (rawUserIds == null)
            {
                return Redirect($"Authentication/Login");
            }
            var order = _context.Orders.FirstOrDefault(u => u.Id.ToString() == id);
            if (order != null)
            {
                order.PaymentStatus = false;
                _context.SaveChanges();
            }
            return RedirectToAction("Index", "Scan");
        }
    }
}
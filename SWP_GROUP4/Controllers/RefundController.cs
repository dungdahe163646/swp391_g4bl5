﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.Order;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class RefundController : Controller
    {
        private readonly FBeautyDbContext _context;
        public RefundController(FBeautyDbContext context)
        { 
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            List<Order> orders = await _context.Orders./*Include(o => o.ProductOrders).ThenInclude( po => po.Product)*/Where( o => o.UserId == userId ).ToListAsync();

            foreach(var item in orders)
            {
                OrderViewModel ovm = new OrderViewModel
                {
                    Id = item.Id,
                    DateCreated = item.DateCreated,
                    PaymentStatus = item.PaymentStatus
                };
            }

            ViewBag.Orders = orders;
            return View();
        }
    }
}

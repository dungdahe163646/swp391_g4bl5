﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Extensions;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.Product;
using SWP_GROUP4.ViewModels.Service;
using SWP_GROUP4.ViewModels.User;
using X.PagedList;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SWP_GROUP4.Controllers
{
    [Authorize(Roles = "UAdmin")]
    public class UDashboardController : Controller
    {
        private readonly FBeautyDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public UDashboardController(FBeautyDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            Dictionary<string, int> UsersInfo = await _context.Rates
                .Include(u => u.User)
                .GroupBy(r => r.User.FirstName + " " + r.User.LastName ?? "Name not specified!")
                .Select( u => new
                {
                    User = u.Key,
                    Count = u.Count()
                })
                .Take(5)
                .OrderByDescending(u => u.Count)
                .ToDictionaryAsync(el => el.User.StringShortener(15), el => el.Count);

            ViewBag.UsersInfo = UsersInfo;
            return View();
        }

        public async Task<ViewResult> ReportManagement(string sortOrder, string currentFilter, string searchString, int? page)
        {
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid reportId = Guid.Parse(id);
            IQueryable<Report> Reports = _context.Reports.Where(u => u.Id != reportId);
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                Reports = Reports.Where(s => s.Email.ToLower().Contains(searchString.ToLower()));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    Reports = Reports.OrderByDescending(p => p.Email);
                    break;
                default:
                    Reports = Reports.OrderBy(p => p.Email);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(Reports.ToPagedList(pageNumber, pageSize));
        }

        public async Task<ViewResult> UserManagement(string sortOrder, string currentFilter, string searchString, int? page)
        {
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(id);
            IQueryable<ApplicationUser> Users = _context.Users.Where(u => u.Id != userId);
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                Users = Users.Where(s => s.LastName.ToLower().Contains(searchString.ToLower()));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    Users = Users.OrderByDescending(p => p.LastName);
                    break;
                default:
                    Users = Users.OrderBy(p => p.LastName);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(Users.ToPagedList(pageNumber, pageSize));
        }

        public async Task<IActionResult> ImageSizeTooLarge()
        {
            return View();
        }

        public async Task<IActionResult> InvalidFormat()
        {
            return View();
        }

        public async Task<IActionResult> DeactiveUser([FromQuery(Name = "Id")] string id)
        {
            Guid userId = Guid.Parse(id);
            ApplicationUser u = await _context.Users.FindAsync(userId);
            u.Status = !u.Status;
            await _context.SaveChangesAsync();
            return Redirect("/UDashboard/UserManagement");
        }

        public async Task<IActionResult> AddOrEditUser([FromQuery(Name = "Id")] string? id)
        {
            UserFormViewModel uvm;
            if (!String.IsNullOrEmpty(id))
            {
                Guid productId = Guid.Parse(id);
                ApplicationUser u = await _context.Users.FindAsync(productId);
                uvm = new()
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    PhoneNumber = u.PhoneNumber,
                    Email = u.Email,
                    Image = u.Image,
                };
            }
            else
            {
                uvm = new();
                uvm.Id = new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
            }
            //List<Company> companies = await _context.Companies.ToListAsync();
            //Dictionary<Guid, string> companySelectors = new();
            //foreach (var item in companies)
            //{
            //    companySelectors.Add(item.Id, item.Name);
            //}
            //ViewBag.CompanySelectors = companySelectors;
            return View(uvm);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrEditProduct(ProductFormViewModel productAddViewModel, IFormFile productImage)
        {
            // check model state of image manually
            ModelState.Remove("Images");
            if (ModelState.IsValid)
            {
                if (productImage.ContentType.ToLower() != "image/jpg" &&
                    productImage.ContentType.ToLower() != "image/jpeg" &&
                    productImage.ContentType.ToLower() != "image/pjpeg" &&
                    productImage.ContentType.ToLower() != "image/gif" &&
                    productImage.ContentType.ToLower() != "image/x-png" &&
                    productImage.ContentType.ToLower() != "image/png")
                {
                    return RedirectToAction("InvalidFormat");
                }
                else if (productImage.Length >= 2097152 || productImage.Length == 0)
                {
                    return RedirectToAction("ImageSizeTooLarge");
                }
                else
                {
                    string pattern = "[^a-zA-Z0-9]";
                    var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/product");
                    var fileName = "product" + Regex.Replace(DateTime.Now.ToString(), pattern, "") + Path.GetExtension(productImage.FileName);
                    var filePath = Path.Combine(imageFolderPath, fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        productImage.CopyTo(stream);
                    }

                    if (productAddViewModel.Id == new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))
                    {
                        Product p = new Product()
                        {
                            Id = Guid.NewGuid(),
                            Name = productAddViewModel.Name,
                            ShortDescription = productAddViewModel.ShortDescription,
                            FullDescription = productAddViewModel.FullDescription,
                            Images = "/img/product/" + fileName,
                            Price = productAddViewModel.Price,
                            Discount = 25,
                            Quantity = 100,
                            IsCombo = true,
                            ReleasedDate = DateTime.Now,
                            Status = true,
                            CompanyId = productAddViewModel.CompanyId,
                            CategoryId = productAddViewModel.CategoryId
                        };
                        await _context.Products.AddAsync(p);
                    }
                    else
                    {
                        Product p = await _context.Products.FirstOrDefaultAsync(p => p.Id == productAddViewModel.Id);
                        p.Name = productAddViewModel.Name;
                        p.ShortDescription = productAddViewModel.ShortDescription;
                        p.FullDescription = productAddViewModel.FullDescription;
                        p.Images = "/img/product/" + fileName;
                        p.Price = productAddViewModel.Price;
                        p.CompanyId = productAddViewModel.CompanyId;
                        p.CategoryId = productAddViewModel.CategoryId;
                    }

                    await _context.SaveChangesAsync();
                    return Redirect("/PDashboard/Index");
                }
            }
            else
            {
                List<Company> companies = await _context.Companies.ToListAsync();
                Dictionary<Guid, string> companySelectors = new();
                foreach (var item in companies)
                {
                    companySelectors.Add(item.Id, item.Name);
                }
                List<Category> categories = await _context.Categories.ToListAsync();
                Dictionary<Guid, string> categorySelectors = new();
                foreach (var item in categories)
                {
                    categorySelectors.Add(item.Id, item.Name);
                }
                ViewBag.CompanySelectors = companySelectors;
                ViewBag.CategorySelectors = categorySelectors;
                return View(productAddViewModel);
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddOrEditUser(UserFormViewModel userFormViewModel, IFormFile userImage)
        {
            // check model state of image manually
            ModelState.Remove("Image");
            if (ModelState.IsValid)
            {
                if (userImage.ContentType.ToLower() != "image/jpg" &&
                    userImage.ContentType.ToLower() != "image/jpeg" &&
                    userImage.ContentType.ToLower() != "image/pjpeg" &&
                    userImage.ContentType.ToLower() != "image/gif" &&
                    userImage.ContentType.ToLower() != "image/x-png" &&
                    userImage.ContentType.ToLower() != "image/png")
                {
                    return RedirectToAction("InvalidFormat");
                }
                else if (userImage.Length >= 2097152 || userImage.Length == 0)
                {
                    return RedirectToAction("ImageSizeTooLarge");
                }
                else
                {

                    string pattern = "[^a-zA-Z0-9]";
                    var imageFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/img/profile");
                    var fileName = "profile" + Regex.Replace(DateTime.Now.ToString(), pattern, "") + Path.GetExtension(userImage.FileName);
                    var filePath = Path.Combine(imageFolderPath, fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        userImage.CopyTo(stream);
                    }
                    if (userFormViewModel.Id == new Guid(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))
                    {
                        ApplicationUser u = new ApplicationUser()
                        {
                            Id = Guid.NewGuid(),
                            FirstName = userFormViewModel.FirstName,
                            LastName = userFormViewModel.LastName,
                            PhoneNumber = userFormViewModel.PhoneNumber,
                            Image = "/img/profile/" + fileName,
                            Email = userFormViewModel.Email,
                            BirthDate = new DateTime(2001, 12, 9),
                            Status = true,
                            About = "Nothing to say",
                            Address = "Vietnam"
                        };
                        await _context.Users.AddAsync(u);
                    }
                    else
                    {
                        ApplicationUser u = await _context.Users.FirstOrDefaultAsync(u => u.Id == userFormViewModel.Id);
                        u.FirstName = userFormViewModel.FirstName;
                        u.LastName = userFormViewModel.LastName;
                        u.Email = userFormViewModel.Email;
                        u.Image = "/img/profile/" + fileName;
                        u.PhoneNumber = userFormViewModel.PhoneNumber;
                    }

                    await _context.SaveChangesAsync();
                    return Redirect("/UDashboard/Index");
                }
            }
            else
            {
                return View(userFormViewModel);
            }
        }
    }
}


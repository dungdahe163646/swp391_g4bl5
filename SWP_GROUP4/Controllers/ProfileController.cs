﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.User;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SWP_GROUP4.Controllers
{
    public class ProfileController : Controller
    {
        private readonly FBeautyDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;


        public ProfileController(FBeautyDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        // GET: /<controller>/
        [Authorize]
        public async Task<IActionResult> Index()
        {
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(id);
            var roleid = _context.UserRoles.FirstOrDefault(u => u.UserId == userId);
            var role = _context.Roles.FirstOrDefault(u => u.Id == roleid.RoleId);
            ApplicationUser rawUser = await _context.Users.FindAsync(userId);
            var userName = User.FindFirstValue(ClaimTypes.Name);
            var userEmail = User.FindFirstValue(ClaimTypes.Email);
            UserViewModel user = new UserViewModel
            {
                FirstName = rawUser.FirstName,
                LastName = rawUser.LastName,
                BirthDate = rawUser.BirthDate,
                About = rawUser.About,
                UserName = userName,
                Email = userEmail,
                Address = rawUser.Address,
                ShipAddress = rawUser.ShipAddress,
                Image = rawUser.Image
            };
            ViewBag.listCategori = _context.Categories.ToList();
            ViewBag.role = role.NormalizedName;
            return View(user);
        }
    }
}


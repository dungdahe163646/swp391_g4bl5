﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels.Product;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private readonly FBeautyDbContext _context;
        List<ProductViewModel> Products = new();

        public CartController(FBeautyDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Cart()
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            LoadCartItem(userId);
            ViewBag.Products = Products;
            return View();
        }

        private void LoadCartItem(Guid userId)
        {
            List<ProductUser> ProductUsers = _context.ProductUsers.Where(pu => pu.UserId == userId).ToList();
            foreach (var item in ProductUsers)
            {
                Product p = _context.Products.FirstOrDefault(p => p.Id == item.ProductId);
                ProductViewModel pvm = new ProductViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    ShortDescription = p.ShortDescription,
                    Images = p.Images,
                    Price = p.Price,
                    Quantity = item.Quantity,
                    Discount = p.Discount
                };
                Products.Add(pvm);
            }
        }

        [HttpPost]
        public IActionResult Cart(string id)
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            LoadCartItem(userId);
            Guid productId = Guid.Parse(id);
            bool isExist = Products.Any(p => p.Id == productId);
            if (isExist)
            {
                var pu = _context.ProductUsers.FirstOrDefault(pu => pu.ProductId == productId && pu.UserId == userId);
                var p = Products.FirstOrDefault(p => p.Id == productId);
                pu.Quantity += 1;
                p.Quantity += 1;
                _context.ProductUsers.Update(pu);
            }
            else
            {
                Product product = _context.Products.Find(productId);
                ProductViewModel pvm = new ProductViewModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    ShortDescription = product.ShortDescription,
                    Images = product.Images,
                    Price = product.Price,
                    Discount = product.Discount
                };
                pvm.Quantity = 1;
                Products.Add(pvm);
                ProductUser productUser = new ProductUser { ProductId = productId, UserId = userId, Quantity = 1 };
                _context.ProductUsers.Add(productUser);
            }
            _context.SaveChanges();
            ViewBag.Products = Products;
            return View();
        }

        [HttpPost]
        public IActionResult RemoveFromCart(Guid productId)
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            
            var productUser = _context.ProductUsers.FirstOrDefault(pu => pu.ProductId == productId && pu.UserId == userId);
            if (productUser != null)
            {
                if (productUser.Quantity > 1)
                {
                    productUser.Quantity -= 1;
                    _context.ProductUsers.Update(productUser);
                }
                else
                {
                    _context.ProductUsers.Remove(productUser);
                }   

                _context.SaveChanges();
            }

            return RedirectToAction("Cart");
        }
        
    }
}

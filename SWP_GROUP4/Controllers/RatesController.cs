﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SWP_GROUP4.Controllers
{
    public class RatesController : Controller
    {
        private readonly FBeautyDbContext _context;

        public RatesController(FBeautyDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> Comment(string id, int rating, string header, string content)
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            Guid productId = Guid.Parse(id);

            // Create a new Rate object
            Rate newRate = new Rate
            {
                ProductId = productId,
                UserId = userId,
                Rating = rating,
                Header = header,
                Content = content,
                CreatedDate = DateTime.Now
            };

            // Save the new rate to the database
            await _context.Rates.AddAsync(newRate);
            await _context.SaveChangesAsync();

            // Redirect to the Index action to show the updated ratings
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Index()
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);

            List<Rate> userRates = await _context.Rates
                .Where(r => r.UserId == userId)
                .ToListAsync();

            List<RateViewModel> userRateViewModels = userRates.Select(rate => new RateViewModel
            {
                Rating = (int) rate.Rating,
                Header = rate.Header,
                Content = rate.Content,
                CreatedDate = rate.CreatedDate
                // Populate other properties as needed
            }).ToList();
            ViewBag.Rates = userRateViewModels;
            return View(userRateViewModels);
        }
    }
}

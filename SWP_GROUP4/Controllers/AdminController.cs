﻿using Microsoft.AspNetCore.Mvc;
using SWP_GROUP4.DbContexts;
using System.Security.Claims;

namespace SWP_GROUP4.Controllers
{
    public class AdminController : Controller
    {
        private readonly ILogger<AdminController> _logger;
        private readonly FBeautyDbContext _context;
        public AdminController(ILogger<AdminController> logger, FBeautyDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        public IActionResult Index()
        {
            string rawUserId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Guid userId = Guid.Parse(rawUserId);
            var roleid = _context.UserRoles.FirstOrDefault(u=>u.UserId== userId);
            var role  = _context.Roles.FirstOrDefault(u=>u.Id == roleid.RoleId);
            if(role.NormalizedName == "PADMIN" || role.NormalizedName == "UADMIN")
            {
                ViewBag.user = userId.ToString();
                return View();
            }
            return NotFound();
        }
    }
}

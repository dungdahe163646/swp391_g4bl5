﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using SWP_GROUP4.Extensions;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.DbContexts
{
    public class FBeautyDbContext : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>
    {
        public FBeautyDbContext()
        {
        }

        public FBeautyDbContext(DbContextOptions<FBeautyDbContext> options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<SpaTreatmentPlan> SpaTreatmentPlans { get; set; }
        public DbSet<ApplicationUser> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<ProductOrder> ProductOrders { get; set; }
        public DbSet<ProductUser> ProductUsers { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Privacy> Privacies { get; set; }
        public DbSet<CompanyTechnology> companyTechnologies { get; set; }   
        public DbSet<Technology> technologies { get; set; }
        public DbSet<ProductTechnology> productTechnologies { get; set; }
        public DbSet<Privacy> privacies { get; set; }   


        public DbSet<SPTUser> SPTUsers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                string? tableName = entityType.GetTableName();
                if (tableName!.StartsWith("AspNet"))
                {
                    entityType.SetTableName(tableName.Substring(6));
                }
            }

            modelBuilder.Entity<Product>(p =>
            {
                p.ToTable("Products");
                p.HasKey(p => p.Id);
                p.HasOne(p => p.Company).WithMany(c => c.Products).HasForeignKey(p => p.CompanyId);
                p.HasOne(p => p.Category).WithMany(c => c.Products).HasForeignKey(p => p.CategoryId);
            });

            modelBuilder.Entity<Administrator>(a =>
            {
                a.ToTable("Administrator");
                a.HasKey(p => p.Id);
                a.HasOne(p => p.Position).WithMany(c => c.Administrators).HasForeignKey(p => p.PositionId);
            });

            modelBuilder.Entity<Position>(p =>
            {
                p.ToTable("Positions");
                p.HasKey(p => p.Id);
            });

            modelBuilder.Entity<Company>(c =>
            {
                c.ToTable("Companies");
                c.HasKey(c => c.Id);
                c.HasOne<ApplicationUser>(c => c.User)
                    .WithOne(u => u.Company)
                    .HasForeignKey<ApplicationUser>(u => u.CompanyId);
            });

            modelBuilder.Entity<Technology>(t =>
            {
                t.ToTable("Technologies");
                t.HasKey(t => t.Id);
                t.HasOne<Company>(t => t.Company)
                    .WithOne(c => c.Technology)
                    .HasForeignKey<Company>(c => c.TechnologyId);
            });

            modelBuilder.Entity<Category>(c =>
            {
                c.ToTable("Categories");
                c.HasKey(c => c.Id);
            });

            modelBuilder.Entity<Report>(r =>
            {
                r.ToTable("Reports");
                r.HasKey(r => r.Id);
            });

            modelBuilder.Entity<Order>(o =>
            {
                o.ToTable("Orders");
                o.HasKey(o => o.Id);
            });

            modelBuilder.Entity<SpaTreatmentPlan>(stp =>
            {
                stp.ToTable("SpaTreatmentPlans");
                stp.HasKey(stp => stp.Id);
                stp.HasOne(stp => stp.Company).WithMany(c => c.SpaTreatmentPlans).HasForeignKey(stp => stp.CompanyId);
            });

            modelBuilder.Entity<Rate>(r =>
            {
                r.ToTable("Rates");
                r.HasKey(r => r.Id);
                r.HasOne(r => r.Product).WithMany(p => p.Rates).HasForeignKey(r => r.ProductId);
            });

            modelBuilder.Entity<Privacy>(p =>
            {
                p.ToTable("Privacies");
                p.HasKey(p => p.Id);
            });

            modelBuilder.Entity<ProductOrder>(po =>
            {
                po.ToTable("ProductOrders");
                po.HasKey(po => new { po.ProductId, po.OrderId });

                po.HasOne(po => po.Product).WithMany(p => p.ProductOrders).HasForeignKey(po => po.ProductId);
                po.HasOne(po => po.Order).WithMany(o => o.ProductOrders).HasForeignKey(po => po.OrderId);
            });

            modelBuilder.Entity<ProductUser>(pu =>
            {
                pu.ToTable("ProductUsers");
                pu.HasKey(pu => new { pu.ProductId, pu.UserId });

                pu.HasOne(pu => pu.Product).WithMany(p => p.ProductUsers).HasForeignKey(pu => pu.ProductId);
                pu.HasOne(pu => pu.User).WithMany(o => o.ProductUsers).HasForeignKey(pu => pu.UserId);
            });

            modelBuilder.Entity<ProductTechnology>(pt =>
            {
                pt.ToTable("ProductTechnologies");
                pt.HasKey(pt => new { pt.ProductId, pt.TechnologyId });

                pt.HasOne(pt => pt.Product).WithMany(p => p.ProductTechnologies).HasForeignKey(pt => pt.ProductId);
                pt.HasOne(pt => pt.Technology).WithMany(t => t.ProductTechnologies).HasForeignKey(pt => pt.TechnologyId).OnDelete(DeleteBehavior.NoAction);
            });

            modelBuilder.Entity<CompanyTechnology>(ct =>
            {
                ct.ToTable("CompanyTechnologies");
                ct.HasKey(ct => new { ct.CompanyId, ct.TechnologyId });

                ct.HasOne(ct => ct.Company).WithMany(c => c.TransferredTechnologies).HasForeignKey(ct => ct.CompanyId);
                ct.HasOne(ct => ct.Technology).WithMany(t => t.TransferredTechnologies).HasForeignKey(ct => ct.TechnologyId).OnDelete(DeleteBehavior.NoAction);

            });

            modelBuilder.Entity<SPTUser>(su =>
            {
                su.ToTable("SPTUsers");
                su.HasKey(ct => new { ct.UserId, ct.STPId });

                su.HasOne(ct => ct.STP).WithMany(c => c.SPTUsers).HasForeignKey(ct => ct.STPId);
                su.HasOne(ct => ct.User).WithMany(t => t.SPTUsers).HasForeignKey(ct => ct.UserId);

            });

            modelBuilder.Seed();
        }
    }
}


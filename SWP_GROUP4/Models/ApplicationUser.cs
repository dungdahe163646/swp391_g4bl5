﻿using System;
using Microsoft.AspNetCore.Identity;

namespace SWP_GROUP4.Models
{
	public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string About { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public bool Status { get; set; }
        public string? ShipAddress { get; set; }
        public string? ResetPasswordToken { get; set; }
        public DateTime? ResetPasswordTokenExpireAt { get; set; }
        public string? OTP { get; set; }
        public DateTime? OTPExpireAt { get; set; }

        public Guid? CompanyId { get; set; }
        public Company? Company { get; set; }

        public virtual ICollection<ProductUser> ProductUsers { get; set; }
        public virtual ICollection<SPTUser> SPTUsers { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
    }
}


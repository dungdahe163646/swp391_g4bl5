﻿using System;
namespace SWP_GROUP4.Models
{
	public class Position : BaseModel
	{
		public string Name { get; set; }
		public string Description { get; set; }

		public virtual ICollection<Administrator> Administrators { get; set; }
	}
}


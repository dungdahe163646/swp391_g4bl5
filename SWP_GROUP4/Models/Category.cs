﻿using System;
namespace SWP_GROUP4.Models
{
	public class Category : BaseModel
	{
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool Status { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}


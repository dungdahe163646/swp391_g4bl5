﻿using System;
namespace SWP_GROUP4.Models
{
	public class ProductOrder
	{
		public int Quantity { get; set; }

		public Guid ProductId { get; set; }
		public Product Product { get; set; }

		public Guid OrderId { get; set; }
		public Order Order { get; set; }
	}
}


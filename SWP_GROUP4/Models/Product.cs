using System;
namespace SWP_GROUP4.Models
{
	public class Product : BaseModel
	{
		public string Name { get; set; }
		public string ShortDescription { get; set; }
		public string FullDescription { get; set; }
		public string Images { get; set; }
		public double Price { get; set; }
		public bool IsCombo { get; set; }
		public int? Quantity { get; set; }
		public int? Discount { get; set; }
		public DateTime ReleasedDate { get; set; }
		public bool Status { get; set; }
		public Guid CompanyId { get; set; }
        public Company Company { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
        public virtual ICollection<ProductUser> ProductUsers { get; set; }
        public virtual ICollection<ProductTechnology> ProductTechnologies { get; set; }
    }
}


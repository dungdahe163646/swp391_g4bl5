﻿using System;
namespace SWP_GROUP4.Models
{
	public class SPTUser
	{
        public DateTime StartDate { get; set; }

        public Guid STPId { get; set; }
        public SpaTreatmentPlan STP { get; set; }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}


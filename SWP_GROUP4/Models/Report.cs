﻿using System;
namespace SWP_GROUP4.Models
{
	public class Report : BaseModel
	{
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Header { get; set; }
		public string Content { get; set; }
		public DateTime ReportedDate { get; set; }
	}
}


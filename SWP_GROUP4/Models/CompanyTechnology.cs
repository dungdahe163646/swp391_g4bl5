﻿using System;
namespace SWP_GROUP4.Models
{
	public class CompanyTechnology
	{
        public Guid CompanyId { get; set; }
        public Company Company { get; set; }
        public int Status { get; set; } 
        public Guid TechnologyId { get; set; }
        public Technology Technology { get; set; }
    }
}


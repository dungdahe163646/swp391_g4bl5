﻿using System;
namespace SWP_GROUP4.Models
{
	public class Technology : BaseModel
	{
		public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string Image { get; set; }

        public Guid? CompanyId { get; set; }
        public Company? Company { get; set; }

        public virtual ICollection<CompanyTechnology> TransferredTechnologies { get; set; }
        public virtual ICollection<ProductTechnology> ProductTechnologies { get; set; }
    }
}


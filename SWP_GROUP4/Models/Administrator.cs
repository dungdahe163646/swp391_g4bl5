﻿using System;
namespace SWP_GROUP4.Models
{
	public class Administrator : BaseModel
	{
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string? ResetPasswordToken { get; set; }
        public DateTime? ResetPasswordTokenExpireAt { get; set; }
        public string? OTP { get; set; }
        public DateTime? OTPExpireAt { get; set; }

        public Guid PositionId { get; set; }
        public Position Position { get; set; }
    }
}
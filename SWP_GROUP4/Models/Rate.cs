﻿using System;
using Microsoft.Extensions.Hosting;

namespace SWP_GROUP4.Models
{
	public class Rate : BaseModel
	{
		public double Rating { get; set; }
		public string Header { get; set; }
		public string Content { get; set; }
		public DateTime CreatedDate { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}


﻿using System;
namespace SWP_GROUP4.Models
{
	public class Company : BaseModel
	{
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string Image { get; set; }
        public string Location { get; set; }
        public bool Status { get; set; }

        public Guid? UserId { get; set; }
        public ApplicationUser? User { get; set; }
        public Guid? TechnologyId { get; set; }
        public Technology? Technology { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<SpaTreatmentPlan> SpaTreatmentPlans { get; set; }
        public virtual ICollection<CompanyTechnology> TransferredTechnologies { get; set; }
    }
}


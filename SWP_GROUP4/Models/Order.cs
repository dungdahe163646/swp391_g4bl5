﻿using System;
namespace SWP_GROUP4.Models
{
	public class Order : BaseModel
	{
		public DateTime DateCreated { get; set; }
		public bool PaymentStatus { get; set; }

		public Guid UserId { get; set; }
		public ApplicationUser User { get; set; }

		public virtual ICollection<ProductOrder> ProductOrders { get; set; }
	}
}


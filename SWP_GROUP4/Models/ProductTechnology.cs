﻿using System;
namespace SWP_GROUP4.Models
{
	public class ProductTechnology
	{
        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public Guid TechnologyId { get; set; }
        public Technology Technology { get; set; }
    }
}


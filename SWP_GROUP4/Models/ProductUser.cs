﻿using System;
namespace SWP_GROUP4.Models
{
	public class ProductUser
	{
        public int Quantity { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}


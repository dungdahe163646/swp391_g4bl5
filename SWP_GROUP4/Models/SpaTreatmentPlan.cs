﻿using System;
namespace SWP_GROUP4.Models
{
	public class SpaTreatmentPlan : BaseModel
	{
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string Images { get; set; }
        public int? Discount { get; set; }
        public DateTime ReleasedDate { get; set; }
        public double Price { get; set; }

        public int Duration { get; set; }
        public bool Status { get; set; }

        public Guid CompanyId { get; set; }
        public Company Company { get; set; }

        public virtual ICollection<SPTUser> SPTUsers { get; set; }
    }
}


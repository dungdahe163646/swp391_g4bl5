﻿namespace SWP_GROUP4.Models
{
    public class TechRequest
    {
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public string Image { get; set; }
        public Guid? CompanyId { get; set; }
        public int Status { get; set; }
        public Guid TechnologyId { get; set; }
    }
}
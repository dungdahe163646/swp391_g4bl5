﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SWP_GROUP4.ViewModels.Report
{
	public class CreateReportViewModel
	{
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Required]
        [MaxLength(50)]
        public string Header { get; set; }
        [Required]
        [MaxLength(500)]
        public string Content { get; set; }
    }
}


﻿using System;
using System.ComponentModel.DataAnnotations;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.Service
{
	public class ServiceFormViewModel: BaseModel
	{
        [Required]
        [MinLength(10)]
        public string Name { get; set; }
        [Required]
        [MinLength(30)]
        public string ShortDescription { get; set; }
        [Required]
        [MinLength(100)]
        public string FullDescription { get; set; }
        public string Images { get; set; }
        //public int? Discount { get; set; }
        public double Price { get; set; }
        public int Duration { get; set; }
        public bool Status { get; set; }
        public Guid CompanyId { get; set; }
    }
}


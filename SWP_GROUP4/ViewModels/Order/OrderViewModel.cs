﻿using Microsoft.AspNetCore.Mvc;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.Order
{
    public class OrderViewModel : Controller
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public bool PaymentStatus { get; set; }
    }
}

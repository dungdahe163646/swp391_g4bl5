﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SWP_GROUP4.ViewModels.User
{
	public class UserConfirmOTPViewModel
	{
        [Required]
        public string OTP { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}


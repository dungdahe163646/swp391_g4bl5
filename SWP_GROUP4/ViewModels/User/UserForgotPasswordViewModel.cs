﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace SWP_GROUP4.ViewModels.User
{
	public class UserForgotPasswordViewModel
	{
        [Required]
        [EmailAddress]
        [Remote("IsExist","Authentication", ErrorMessage = "Email isn't exist")]
        public string Email { get; set; }
    }
}


﻿using System;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.User
{
	public class UserFormViewModel: BaseModel
	{
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }

        //public Guid? CompanyId { get; set; }
    }
}


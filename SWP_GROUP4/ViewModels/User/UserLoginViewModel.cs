﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SWP_GROUP4.ViewModels.User
{
	public class UserLoginViewModel
	{
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}


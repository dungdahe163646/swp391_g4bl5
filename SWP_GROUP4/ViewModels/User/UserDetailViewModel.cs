﻿using System;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.User
{
	public class UserDetailViewModel : ApplicationUser
	{
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string RoleName { get; set; }
	}
}


﻿using System;
namespace SWP_GROUP4.ViewModels.User
{
	public class UserViewModel
	{
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string? ShipAddress { get; set; }
        public DateTime BirthDate { get; set; }
        public string About { get; set; }
    }
}


﻿using System;
using System.ComponentModel.DataAnnotations;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.Product
{
	public class ProductFormViewModel : BaseModel
	{
        [Required]
        [MinLength(10)]
        public string Name { get; set; }
        [Required]
        [MinLength(30)]
        public string ShortDescription { get; set; }
        [Required]
        [MinLength(100)]
        public string FullDescription { get; set; }
        public string Images { get; set; }
        public double Price { get; set; }
        //public bool IsCombo { get; set; }
        //public int? Quantity { get; set; }
        //public int? Discount { get; set; }
        [Required]
        public Guid CompanyId { get; set; }
        [Required]
        public Guid CategoryId { get; set; }
        //public virtual ICollection<ProductTechnology> ProductTechnologies { get; set; }
    }
}


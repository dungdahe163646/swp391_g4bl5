﻿namespace SWP_GROUP4.ViewModels.Product
{
    public class ProductViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }      
        public string Images { get; set; }
        public double Price { get; set; }      
        public int Quantity { get; set; }
        public int? Discount { get; set; }
    }
}

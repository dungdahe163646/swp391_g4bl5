﻿
using System;
using System.ComponentModel.DataAnnotations;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.Category
{
	public class CategoryFormViewModel : BaseModel
	{
        [Required]
        [MinLength(10)]
        public string Name { get; set; }
        [Required]
        [MinLength(100)]
        public string Description { get; set; }
        public bool Status { get; set; }
    }
}


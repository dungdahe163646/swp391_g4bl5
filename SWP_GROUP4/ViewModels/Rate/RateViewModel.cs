﻿namespace SWP_GROUP4.ViewModels
{
    public class RateViewModel
    {
        public double Rating { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public string UserImage { get; set; }
        public string UserFullname { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}

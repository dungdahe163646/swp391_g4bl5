﻿using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.Company
{
    public class CompanyTechnologyViewModel
    {
        public List<CompanyTechnology> Technologies { get; set; }
    }
}

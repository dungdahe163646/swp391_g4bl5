﻿using System;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.ViewModels.Company
{
	public class CompanySelector:BaseModel
	{
		public string Name { get; set; }
	}
}


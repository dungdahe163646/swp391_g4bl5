using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;
using SWP_GROUP4.Services;

namespace SWP_GROUP4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            builder.Services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
            });
            builder.Services.AddDbContext<FBeautyDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("FBeautyDB")));
            builder.Services.AddScoped<IEmailServices, EmailServices>();
            builder.Services.AddScoped<IUserServices, UserServices>();
            builder.Services.AddIdentity<ApplicationUser, IdentityRole<Guid>>(options => {
                options.SignIn.RequireConfirmedAccount = false;
            })
                    .AddEntityFrameworkStores<FBeautyDbContext>()
                    .AddDefaultTokenProviders();

            builder.Services.ConfigureApplicationCookie(options => {
                // options.Cookie.HttpOnly = true;
                // options.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                options.LoginPath = "/Authentication/Index";
                options.LogoutPath = "/Authentication/Logout";
                options.AccessDeniedPath = "/Home/AccessDenied";
            });

            builder.Services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "Credentials";
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);
                options.SlidingExpiration = true;
            });

            var app = builder.Build();
            app.UseSession(); 
            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");

            app.Run();
        }
    }
}
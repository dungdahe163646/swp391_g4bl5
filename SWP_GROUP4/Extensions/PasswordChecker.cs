﻿using System;
using System.Text.RegularExpressions;

namespace SWP_GROUP4.Extensions
{
	public static class PasswordChecker
	{
        const string PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";

        public static bool Check(string password)
		{
			if(Regex.IsMatch(password, PASSWORD_REGEX))
			{
				return true;
			}
			return false;
		}
	}
}


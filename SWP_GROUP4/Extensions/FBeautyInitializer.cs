﻿using System;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SWP_GROUP4.Models;
using System.Collections.Immutable;

namespace SWP_GROUP4.Extensions
{  
    public static class FBeautyInitializer
    {
        public static void Seed(this ModelBuilder builder)
        {
            var hasher = new PasswordHasher<ApplicationUser>();
            var users = new ApplicationUser[]
{
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Vendor",
                    LastName = "System",
                    BirthDate = new DateTime(2002,1,1),
                    About = "Nothing to say",
                    UserName = "vendor",
                    NormalizedUserName = "VENDOR",
                    Email = "vendor@localhost.com",
                    Image = "/img/profile/avatar_default.jpg",
                    Address = "Hanoi, Vietnam",
                    NormalizedEmail = "VENDOR@LOCALHOST.COM",
                    EmailConfirmed = true,
                    PhoneNumber = "+84 123456789",
                    AccessFailedCount = 0,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "P@ssw000000rd")
                },
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Customer",
                    LastName = "System",
                    BirthDate = new DateTime(2002,1,1),
                    About = "Nothing to say",
                    UserName = "customer",
                    NormalizedUserName = "CUSTOMER",
                    Email = "customer@localhost.com",
                    Image = "/img/profile/avatar_default.jpg",
                    Address = "Hanoi, Vietnam",
                    NormalizedEmail = "CUSTOMER@LOCALHOST.COM",
                    EmailConfirmed = true,
                    PhoneNumber = "+84 123456987",
                    AccessFailedCount = 0,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "P@ssw000000rd")
                },
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "user1",
                    LastName = "user1",
                    BirthDate = new DateTime(2002,1,1),
                    About = "Nothing to say",
                    UserName = "user1",
                    NormalizedUserName = "User1",
                    Email = "user1@localhost.com",
                    Image = "/img/profile/avatar_default.jpg",
                    Address = "Hanoi, Vietnam",
                    NormalizedEmail = "USER1@LOCALHOST.COM",
                    EmailConfirmed = true,
                    PhoneNumber = "+84 123456987",
                    AccessFailedCount = 0,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "P@ssw000000rd")
                },
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "user2",
                    LastName = "usr2",
                    BirthDate = new DateTime(2002,1,1),
                    About = "Nothing to say",
                    UserName = "user2",
                    NormalizedUserName = "User2",
                    Email = "user2@localhost.com",
                    Image = "/img/profile/avatar_default.jpg",
                    Address = "Hanoi, Vietnam",
                    NormalizedEmail = "USER2@LOCALHOST.COM",
                    EmailConfirmed = true,
                    PhoneNumber = "+84 123456987",
                    AccessFailedCount = 0,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "P@ssw000000rd")
                },
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "vendor2",
                    LastName = "vendor2",
                    BirthDate = new DateTime(2002,1,1),
                    About = "Nothing to say",
                    UserName = "vendor2",
                    NormalizedUserName = "Vendor2",
                    Email = "vendor2@localhost.com",
                    Image = "/img/profile/avatar_default.jpg",
                    Address = "Hanoi, Vietnam",
                    NormalizedEmail = "VENDOR2@LOCALHOST.COM",
                    EmailConfirmed = true,
                    PhoneNumber = "+84 123456987",
                    AccessFailedCount = 0,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "P@ssw000000rd")
                },
                new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    FirstName = "test2",
                    LastName = "test2",
                    BirthDate = new DateTime(2002,1,1),
                    About = "Nothing to say",
                    UserName = "test2",
                    NormalizedUserName = "Test2",
                    Email = "test2@localhost.com",
                    Image = "/img/profile/avatar_default.jpg",
                    Address = "Hanoi, Vietnam",
                    NormalizedEmail = "TEST2@LOCALHOST.COM",
                    EmailConfirmed = true,
                    PhoneNumber = "+84 123456987",
                    AccessFailedCount = 0,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(null, "P@ssw000000rd")
                },
};
            var categories = new Category[]
            {
                new Category
                {
                    Id = Guid.NewGuid(),
                    Name = "Makeup",
                    Description = "Discover Pat McGrath Labs, Morphe x Meredith Duxbury, Half Magic and more.",
                    Image = "/Images/Category_Makeup.jpeg",
                    Status = true
                },
                new Category
                {
                    Id = Guid.NewGuid(),
                    Name = "Skin care",
                    Description = "Fresh picks from Tula, Dermalogica and more.",
                    Image = "/Images/Category_SkinCare.jpeg",
                    Status = true
                },
                new Category
                {
                    Id = Guid.NewGuid(),
                    Name = "Hair care",
                    Description = "Add new Redken and Drybar to your routine.",
                    Image = "/Images/Category_HairCare.png",
                    Status = true
                },
                new Category
                {
                    Id = Guid.NewGuid(),
                    Name = "Fragrance",
                    Description = "Explore soon-to-be faves from Coach, Gucci, Burberry and more.",
                    Image = "/Images/Category_Fragrance.jpeg",
                    Status = true
                }
            };
            var techlonogies = new Technology[]
{
               new Technology
               {
                   Id = Guid.NewGuid(),
                   Name = "Skin Care Tech",
                   ShortDescription = "So many skin-care devices catch our eye that it’s hard to tell the truly helpful from the purely hyped. So we asked the professionals — four dermatologists and two aestheticians, to be exact — about the best options for a number of skin concerns including acne, wrinkles, and hyperpigmentation",
                   FullDescription = "Whether you’re already happy with your skin and just want a personalized routine to maintain it, or have some serious goals you’d like to meet, our experts shared the best beauty tech out there — including facial brushes to get a deeper clean and red-light helmets to promote hair growth. We’re focusing only on the higher investment–higher payoff beauty tech so we haven’t included manual tools (like gua sha stones). And because these devices use various forms of energy like LED light or microcurrent technology, consult your doctor before using if you’re pregnant or have an existing medical condition.",
                   Image = "/img/tech/tech1.jpg",
               },
               new Technology
               {
                   Id = Guid.NewGuid(),
                   Name = "Hair Care Tech",
                   ShortDescription = "So many skin-care hairs devices catch our eye that it’s hard to tell the truly helpful from the purely hyped. So we asked the professionals — four dermatologists and two aestheticians, to be exact — about the best options for a number of skin concerns including acne, wrinkles, and hyperpigmentation",
                   FullDescription = "Whether you’re already happy with your skin and just want a personalized routine to maintain it, or have some serious goals you’d like to meet, our experts shared the best beauty tech out there — including facial brushes to get a deeper clean and red-light helmets to promote hair growth. We’re focusing only on the higher investment–higher payoff beauty tech so we haven’t included manual tools (like gua sha stones). And because these devices use various forms of energy like LED light or microcurrent technology, consult your doctor before using if you’re pregnant or have an existing medical condition.",
                   Image = "/img/tech/tech2.jpg",
               }
               ,
               new Technology
               {
                   Id = Guid.NewGuid(),
                   Name = "Body Care Tech",
                   ShortDescription = "So many skin-care bodys devices catch our eye that it’s hard to tell the truly helpful from the purely hyped. So we asked the professionals — four dermatologists and two aestheticians, to be exact — about the best options for a number of skin concerns including acne, wrinkles, and hyperpigmentation",
                   FullDescription = "Whether you’re already happy with your skin and just want a personalized routine to maintain it, or have some serious goals you’d like to meet, our experts shared the best beauty tech out there — including facial brushes to get a deeper clean and red-light helmets to promote hair growth. We’re focusing only on the higher investment–higher payoff beauty tech so we haven’t included manual tools (like gua sha stones). And because these devices use various forms of energy like LED light or microcurrent technology, consult your doctor before using if you’re pregnant or have an existing medical condition.",
                   Image = "/img/tech/tech1.jpg",
               },
               new Technology
               {
                   Id = Guid.NewGuid(),
                   Name = "Eyes Care Tech",
                   ShortDescription = "So many skin-care eyesss devices catch our eye that it’s hard to tell the truly helpful from the purely hyped. So we asked the professionals — four dermatologists and two aestheticians, to be exact — about the best options for a number of skin concerns including acne, wrinkles, and hyperpigmentation",
                   FullDescription = "Whether you’re already happy with your skin and just want a personalized routine to maintain it, or have some serious goals you’d like to meet, our experts shared the best beauty tech out there — including facial brushes to get a deeper clean and red-light helmets to promote hair growth. We’re focusing only on the higher investment–higher payoff beauty tech so we haven’t included manual tools (like gua sha stones). And because these devices use various forms of energy like LED light or microcurrent technology, consult your doctor before using if you’re pregnant or have an existing medical condition.",
                   Image = "/img/tech/tech2.jpg",
               }
};

            var companies = new Company[]{
                new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "Estée Lauder",
                    ShortDescription = "The Estée Lauder Companies Inc. is one of the world's leading manufacturers, marketers, and sellers of quality skin care, makeup, fragrance, and hair care products, and is a steward of outstanding luxury and prestige brands globally.",
                    FullDescription = "The company’s products are sold in approximately 150 countries and territories under brand names including: Estée Lauder, Aramis, Clinique, Lab Series, Origins, M·A·C, La Mer, Bobbi Brown Cosmetics, Aveda, Jo Malone London, Bumble and bumble, Darphin Paris, TOM FORD, Smashbox, AERIN Beauty, Le Labo, Editions de Parfums Frédéric Malle, GLAMGLOW, KILIAN PARIS, Too Faced, Dr.Jart+, and the DECIEM family of brands, including The Ordinary and NIOD. The Estée Lauder Companies sells products in approximately 150 countries and territories. We have three main geographic regions: Asia/Pacific; Europe, the Middle East & Africa; and The Americas. Each region is composed of one or more affiliates. Today we have affiliates in 50+ countries and territories.",
                    Image = "/img/companie/comp1.jpg",
                    Location = "New York City's General Motors building",
                    Status = true,
                    UserId = users[0].Id,
                      TechnologyId = techlonogies[2].Id
                },
                new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "Lauder Estée",
                    ShortDescription = "The Estée Lauder Companies Inc. is one of the world's leading manufacturers, marketers, and sellers of quality skin care, makeup, fragrance, and hair care products, and is a steward of outstanding luxury and prestige brands globally.",
                    FullDescription = "The company’s products are sold in approximately 150 countries and territories under brand names including: Estée Lauder, Aramis, Clinique, Lab Series, Origins, M·A·C, La Mer, Bobbi Brown Cosmetics, Aveda, Jo Malone London, Bumble and bumble, Darphin Paris, TOM FORD, Smashbox, AERIN Beauty, Le Labo, Editions de Parfums Frédéric Malle, GLAMGLOW, KILIAN PARIS, Too Faced, Dr.Jart+, and the DECIEM family of brands, including The Ordinary and NIOD. The Estée Lauder Companies sells products in approximately 150 countries and territories. We have three main geographic regions: Asia/Pacific; Europe, the Middle East & Africa; and The Americas. Each region is composed of one or more affiliates. Today we have affiliates in 50+ countries and territories.",
                    Image = "/img/companie/comp2.jpg",
                    Location = "New York City's General Motors building",
                    Status = true,
                     UserId = users[1].Id,
                      TechnologyId = techlonogies[3].Id
                },
                 new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "User1 Companies Skin Care",
                    ShortDescription = "The Estée Lauder Companies Inc. is one of the world's leading manufacturers, marketers, and sellers of quality skin care, makeup, fragrance, and hair care products, and is a steward of outstanding luxury and prestige brands globally.",
                    FullDescription = "The company’s products are sold in approximately 150 countries and territories under brand names including: Estée Lauder, Aramis, Clinique, Lab Series, Origins, M·A·C, La Mer, Bobbi Brown Cosmetics, Aveda, Jo Malone London, Bumble and bumble, Darphin Paris, TOM FORD, Smashbox, AERIN Beauty, Le Labo, Editions de Parfums Frédéric Malle, GLAMGLOW, KILIAN PARIS, Too Faced, Dr.Jart+, and the DECIEM family of brands, including The Ordinary and NIOD. The Estée Lauder Companies sells products in approximately 150 countries and territories. We have three main geographic regions: Asia/Pacific; Europe, the Middle East & Africa; and The Americas. Each region is composed of one or more affiliates. Today we have affiliates in 50+ countries and territories.",
                    Image = "/img/companie/comp3.jpg",
                    Location = "New York City's General Motors building",
                    Status = true,
                    UserId = users[2].Id,
                    TechnologyId = techlonogies[0].Id
                },
                new Company
                {
                    Id = Guid.NewGuid(),
                    Name = "User2 Companies Hair Cair",
                    ShortDescription = "The Estée Lauder Companies Inc. is one of the world's leading manufacturers, marketers, and sellers of quality skin care, makeup, fragrance, and hair care products, and is a steward of outstanding luxury and prestige brands globally.",
                    FullDescription = "The company’s products are sold in approximately 150 countries and territories under brand names including: Estée Lauder, Aramis, Clinique, Lab Series, Origins, M·A·C, La Mer, Bobbi Brown Cosmetics, Aveda, Jo Malone London, Bumble and bumble, Darphin Paris, TOM FORD, Smashbox, AERIN Beauty, Le Labo, Editions de Parfums Frédéric Malle, GLAMGLOW, KILIAN PARIS, Too Faced, Dr.Jart+, and the DECIEM family of brands, including The Ordinary and NIOD. The Estée Lauder Companies sells products in approximately 150 countries and territories. We have three main geographic regions: Asia/Pacific; Europe, the Middle East & Africa; and The Americas. Each region is composed of one or more affiliates. Today we have affiliates in 50+ countries and territories.",
                    Image = "/img/companie/comp4.jpg",
                    Location = "New York City's General Motors building",
                    Status = true,
                    UserId = users[3].Id,
                    TechnologyId = techlonogies[1].Id
                }
            };


            var products = new Product[] {
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear Stay-in-Place Foundation",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product1.jpg",
                    Price = 100,
                    Discount = 25,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },
                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product2.jpg",
                    Price = 456,
                    Discount = 70,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[1].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear2 ",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product3.jpg",
                    Price = 743,
                    Discount = 0,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[1].Id,
                    CategoryId = categories[2].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear3 ",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product4.jpg",
                    Price = 234,
                    Discount = 0,
                    Quantity =7,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[1].Id,
                    CategoryId = categories[3].Id
                },
                   new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear Stay-in-Place Foundation",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product5.jpg",
                    Price = 563,
                    Discount = 25,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },
                                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product6.jpg",
                    Price = 256,
                    Discount = 70,
                    Quantity =100,
                    IsCombo = false,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[1].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear2 ",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product7.jpg",
                    Price = 987,
                    Discount = 0,
                    Quantity =100,
                    IsCombo = false,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[1].Id,
                    CategoryId = categories[2].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear3 ",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product8.jpg",
                    Price = 122,
                    Discount = 0,
                    Quantity =7,
                    IsCombo = false,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[1].Id,
                    CategoryId = categories[3].Id
                },
                   new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear Stay-in-Place Foundation",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product9.jpg",
                    Price = 553,
                    Discount = 25,
                    Quantity =100,
                    IsCombo = false,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },
                                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product10.jpg",
                    Price = 532,
                    Discount = 75,
                    Quantity =100,
                    IsCombo = false,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[1].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear2 ",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product11.jpg",
                    Price = 987,
                    Discount = 0,
                    Quantity =100,
                    IsCombo = false,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[1].Id,
                    CategoryId = categories[2].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear3 ",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product12.jpg",
                    Price = 123,
                    Discount = 0,
                    Quantity =7,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[1].Id,
                    CategoryId = categories[3].Id
                },
                   new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear Stay-in-Place Foundation",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product13.jpg",
                    Price = 600,
                    Discount = 25,
                    Quantity =100,
                    IsCombo = false,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },
                                new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Double Wear",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product14.jpg",
                    Price = 500,
                    Discount = 50,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },
                                 new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Skin Hair1",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product15.jpg",
                    Price = 500,
                    Discount = 50,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Skin Care2",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product16.jpg",
                    Price = 500,
                    Discount = 50,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[2].Id,
                    CategoryId = categories[0].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Hair Care1",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product17.jpg",
                    Price = 500,
                    Discount = 50,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },
                  new Product
                {
                    Id = Guid.NewGuid(),
                    Name = "Hair Care2",
                    ShortDescription = "Wear confidence. Estée Lauder Double Wear Stay-in-Place Makeup is a 24-hour liquid foundation with a flawless, natural, matte finish that helps to unify uneven skin tone and control oil and shine. Buildable medium-to-full coverage in over 55 shades.",
                    FullDescription = "Elevate your self-assurance and embrace a radiant demeanor with Estée Lauder Double Wear Stay-in-Place Makeup. This exceptional 24-hour liquid foundation stands as a testament to enduring beauty, offering a seamless and impeccable matte finish that effortlessly harmonizes even the most disparate skin tones. Crafted to empower you with unwavering poise, it goes beyond a mere cosmetic product, becoming a reliable ally in your daily routine.\n\nUnveil a transformational experience as this foundation effortlessly blends onto your skin, concealing imperfections while revealing your innate allure. Its advanced formula not only imbues your complexion with a velvety smoothness but also combats excess oil and luminous sheen, bestowing you with a velvety, shine-free visage throughout the day.\n\nWith its adaptability and versatility, you possess the ability to customize your desired coverage level, ranging from a subtle enhancement to a full-fledged, flawlessly sophisticated guise. Choose from a vast palette of over 55 shades, ensuring an impeccable match for your unique complexion. The careful curation of shades underscores Estée Lauder's commitment to celebrating the beauty of diversity, enabling you to express your individuality and authenticity effortlessly.\n\nIndulge in the symphony of elegance and resilience, as Estée Lauder Double Wear Stay-in-Place Makeup becomes your steadfast companion on your journey to exude confidence and radiate an aura of timeless allure. Embark on a transformative odyssey, where every application becomes a brushstroke of self-assuredness, painting the canvas of your visage with the hues of empowerment and grace.",
                    Images = "/img/product/product18.jpg",
                    Price = 500,
                    Discount = 50,
                    Quantity =100,
                    IsCombo = true,
                    ReleasedDate = new DateTime(2018, 12, 07),
                    Status = true,
                    CompanyId = companies[0].Id,
                    CategoryId = categories[0].Id
                },

            };

            var roles = new IdentityRole<Guid>[]
                {
                    new IdentityRole<Guid>
                    {
                        Id = Guid.NewGuid(),
                        Name = "Vendor",
                        NormalizedName = "VENDOR"
                    },
                    new IdentityRole<Guid>
                    {
                        Id = Guid.NewGuid(),
                        Name = "User",
                        NormalizedName = "USER"
                    },
                    new IdentityRole<Guid>
                    {
                        Id = Guid.NewGuid(),
                        Name = "PAdmin",
                        NormalizedName = "PADMIN"
                    },
                    new IdentityRole<Guid>
                    {
                        Id = Guid.NewGuid(),
                        Name = "UAdmin",
                        NormalizedName = "UADMIN"
                },
            };

            var orders = new Order[] {
                new Order
                {
                    Id = Guid.NewGuid(),
                    PaymentStatus = true,
                    UserId = users[0].Id
                }
            };

            var pos = new ProductOrder[] {
                new ProductOrder{
                    Quantity = 1,
                    OrderId = orders[0].Id,
                    ProductId = products[0].Id
                },

            };

            //var pcs = new ProductCategory[]
            //{
            //    new ProductCategory
            //    {
            //        ProductId = products[0].Id,
            //        CategoryId = categories[0].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[1].Id,
            //        CategoryId = categories[1].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[2].Id,
            //        CategoryId = categories[2].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[3].Id,
            //        CategoryId = categories[3].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[4].Id,
            //        CategoryId = categories[1].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[5].Id,
            //        CategoryId = categories[0].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[6].Id,
            //        CategoryId = categories[2].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[7].Id,
            //        CategoryId = categories[3].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[8].Id,
            //        CategoryId = categories[0].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[9].Id,
            //        CategoryId = categories[1].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[10].Id,
            //        CategoryId = categories[2].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[11].Id,
            //        CategoryId = categories[3].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[12].Id,
            //        CategoryId = categories[0].Id
            //    },
            //    new ProductCategory
            //    {
            //        ProductId = products[13].Id,
            //        CategoryId = categories[1].Id
            //    },
            //};

            var pus = new ProductUser[]
            {
                new ProductUser
                {
                    ProductId = products[0].Id,
                    UserId = users[0].Id,
                    Quantity = 4
                },
                new ProductUser
                {
                    ProductId = products[1].Id,
                    UserId = users[0].Id,
                },
                new ProductUser
                {
                    ProductId = products[2].Id,
                   UserId = users[0].Id,
                    Quantity = 5
                },                new ProductUser
                {
                    ProductId = products[3].Id,
                   UserId = users[0].Id,
                    Quantity = 7
                },
                new ProductUser
                {
                    ProductId = products[4].Id,
                    UserId = users[0].Id,
                },
                new ProductUser
                {
                    ProductId = products[5].Id,
                    UserId = users[0].Id,
                    Quantity = 8
                },
                new ProductUser
                {
                    ProductId = products[6].Id,
                    UserId = users[0].Id,
                    Quantity = 1
                },





                // new ProductUser
                //{
                //    ProductId = products[7].Id,
                //    UserId = users[1].Id,
                //    Quantity = 4
                //},
                //new ProductUser
                //{
                //    ProductId = products[8].Id,
                //    UserId = users[1].Id,
                //},
                //new ProductUser
                //{
                //    ProductId = products[9].Id,
                //   UserId = users[1].Id,
                //    Quantity = 5
                //},                new ProductUser
                //{
                //    ProductId = products[10].Id,
                //   UserId = users[1].Id,
                //    Quantity = 7
                //},
                //new ProductUser
                //{
                //    ProductId = products[11].Id,
                //    UserId = users[1].Id,
                //},
                //new ProductUser
                //{
                //    ProductId = products[12].Id,
                //    UserId = users[1].Id,
                //    Quantity = 8
                //},
                //new ProductUser
                //{
                //    ProductId = products[13].Id,
                //    UserId = users[1].Id,
                //    Quantity = 1
                //},
            };

            var stps = new SpaTreatmentPlan[]
            {
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "CHARME D’ORIENT BODY SCRUB TREATMENT",
                    ShortDescription = "We offer the choice of two different types of body polish for a delicate polishing of the body, paying special attention to your back as well as rougher areas like your feet, knees and elbows.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Điều Trị Nám",
                    ShortDescription = "Mang lại vẻ đẹp tự nhiên mà không lo sợ nám quay lại nữa.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Dịch vụ triệt lông",
                    ShortDescription = "Dịch vụ triệt lông vĩnh viễn đảm bảo cho bạn một làn da sáng hơn",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Điều trị mụn",
                    ShortDescription = "Đánh bay nỗi lo mụn trên khuôn mặt bạn và tự tin tỏa sáng hơn",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Trẻ hóa làn da",
                    ShortDescription = "Dịch vụ trẻ hóa làn da, giúp bạn sở hữu làn da như trẻ hơn 10 tuổi",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Điều trị thâm",
                    ShortDescription = "Xóa thâm, trị tái phát lại, đem lại vẻ đẹp hoàn hảo cho bạn.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Tắm trắng",
                    ShortDescription = "Các gói tắm trắng thảo dược trị liệu giúp bạn trắng sáng hơn.",
                    FullDescription = "Tắm trắng là phương pháp sử dụng hóa chất lột bỏ lớp da chết bên ngoài nằm trong lớp á sừng. Trong khi đó, các tế bào hắc tố melanin vẫn nằm sâu trong lớp mầm của thượng bì và liên tục được sản sinh khiến làn da trở lại trạng thái đen xỉn ban đầu. Nếu sử dụng phương pháp này thường xuyên có thể giúp làn da “sáng” hơn do các lớp tế bào chết bên ngoài được tẩy liên tục, nhưng chỉ là sáng da tạm thời bởi một tế bào da có tuổi thọ từ 28 - 40 ngày,nên sau thời gian đó làn da lại thay thế lớp tế bào chết khác, khiến da lại đen trở lại.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Trị viêm nang lông",
                    ShortDescription = "Điều trị dứt điểm bệnh viêm nang lông, giúp bạn tự tin tỏa sáng.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "Xóa xăm, chàm, bớt",
                    ShortDescription = "Tẩy xóa các hình xăm, chàm, bớt an toàn và không để lại sẹo.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "CHARME D’ORIENT BODY SCRUB TREATMENT",
                    ShortDescription = "We offer the choice of two different types of body polish for a delicate polishing of the body, paying special attention to your back as well as rougher areas like your feet, knees and elbows.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "CHARME D’ORIENT BODY SCRUB TREATMENT",
                    ShortDescription = "We offer the choice of two different types of body polish for a delicate polishing of the body, paying special attention to your back as well as rougher areas like your feet, knees and elbows.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
                new SpaTreatmentPlan
                {
                    Id = Guid.NewGuid(),
                    Name = "CHARME D’ORIENT BODY SCRUB TREATMENT",
                    ShortDescription = "We offer the choice of two different types of body polish for a delicate polishing of the body, paying special attention to your back as well as rougher areas like your feet, knees and elbows.",
                    FullDescription = "Embrace a pampering experience like no other with our exclusive selection of body polishes, tailored to cater to your distinct needs. Our discerning clientele is presented with a choice between two exquisite types of body polish, each designed to delicately refine and rejuvenate your skin, leaving you with a luxurious glow that's bound to turn heads.\n\nOur meticulous approach extends beyond the ordinary, as we pay meticulous attention to every inch of your body. Envision a tranquil escape where your back receives the devoted care it deserves, ensuring that no area is overlooked in your quest for complete radiance. Moreover, those tougher zones that often require extra care, such as your feet, knees, and elbows, are treated with the utmost precision, transforming them from rough to irresistibly smooth.\n\nIndulge in the opulent sensation of our body polishes as they sweep away impurities, unveiling a velvety softness that invites both touch and admiration. The unique formulation of each polish caters to the diverse aspects of your skin's texture, exfoliating and refining without compromising its delicate balance.\n\nElevate your self-care ritual by choosing the ideal body polish for your preferences, as we invite you to experience the artistry of pampering perfected through years of expertise. With our commitment to your well-being and beauty, you'll discover a newfound appreciation for your skin's natural brilliance, as well as the transformative power of personalized care.",
                    Images = "",
                    ReleasedDate = new DateTime(2021, 1, 16),
                    Price = 240,
                    Duration = 180,
                    Status = true,
                    CompanyId = companies[0].Id
                },
            };

            var companiesTechnology = new CompanyTechnology[]{
                new CompanyTechnology
                {
                   TechnologyId = techlonogies[0].Id,
                   CompanyId= companies[2].Id,
                   Status =1
                },

                new CompanyTechnology
                {
                   TechnologyId = techlonogies[1].Id,
                   CompanyId= companies[3].Id,
                   Status = 1
                },
                new CompanyTechnology
                {
                   TechnologyId = techlonogies[2].Id,
                   CompanyId= companies[0].Id,
                   Status = 1
                },

                new CompanyTechnology
                {
                   TechnologyId = techlonogies[3].Id,
                   CompanyId= companies[1].Id,
                   Status = 1
                }
            };

            var productTechlonogies = new ProductTechnology[]
            {
                new ProductTechnology
                {
                    ProductId = products[14].Id,
                    TechnologyId= techlonogies[0].Id,
                },
                 new ProductTechnology
                {
                    ProductId = products[15].Id,
                    TechnologyId= techlonogies[0].Id,
                },
                 new ProductTechnology
                {
                    ProductId = products[16].Id,
                    TechnologyId= techlonogies[1].Id,
                },
                 new ProductTechnology
                {
                    ProductId = products[17].Id,
                    TechnologyId= techlonogies[1].Id,
                }
            };

            var iurs = new IdentityUserRole<Guid>[] {
                new IdentityUserRole<Guid>
                {
                    RoleId = roles[0].Id,
                    UserId = users[0].Id
                },
                new IdentityUserRole<Guid>
                {
                    RoleId = roles[1].Id,
                    UserId = users[1].Id
                },
                new IdentityUserRole<Guid>
                {
                    RoleId = roles[0].Id,
                    UserId = users[2].Id
                },
                new IdentityUserRole<Guid>
                {
                    RoleId = roles[0].Id,
                    UserId = users[3].Id
                },
                new IdentityUserRole<Guid>
                {
                    RoleId = roles[0].Id,
                    UserId = users[4].Id
                },
                new IdentityUserRole<Guid>
                {
                    RoleId = roles[0].Id,
                    UserId = users[5].Id
                },
            };

            var reports = new Report[] {
                new Report
                {
                    Id = Guid.NewGuid(),
                    Email = "guest@localhost.com",
                    Phone = "+84 84848484",
                    Header = "Good products and services",
                    Content = "This website provide very good products and services, it will be better if owner of this website improve it performance. Thanks!",
                    ReportedDate = DateTime.Now
                }
            };

            var positions = new Position[]
            {
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "Product Manager",
                    Description = "Manage Product"
                },
                new Position
                {
                    Id = Guid.NewGuid(),
                    Name = "User Manager",
                    Description = "Manage User"
                }
            };
            var privaci = new Privacy[]
            {
                new Privacy
                {
                    Id = Guid.NewGuid(),
                    Version = "A website’s privacy policy",
                    Content = "outlines if and how you collect, use, share, or sell your visitors’ personal information and is required under laws like the General Data Privacy Regulation (GDPR) and the California Consumer Privacy Act (CCPA).\r\n\r\nKeep reading to learn more about privacy policies, why you need one for your website, see some examples, and download our free privacy policy template."
                },
                new Privacy
                {
                    Id = Guid.NewGuid(),
                    Version = "A website’s privacy policy Ver2",
                    Content = "outlines if and how you collect, use, share, or sell your visitors’ personal information and is required under laws like the General Data Privacy Regulation (GDPR) and the California Consumer Privacy Act (CCPA).\r\n\r\nKeep reading to learn more about privacy policies, why you need one for your website, see some examples, and download our free privacy policy template."
                }
            };

            byte[] salt = new byte[] { (byte)'a', (byte)'d', (byte)'m', (byte)'i', (byte)'n' };


            var admins = new Administrator[]
            {
                new Administrator
                {
                    Id = Guid.NewGuid(),
                    Email = "datndw@gmail.com",
                    PasswordHash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                        password: "P@ssw000000rd",
                        salt: salt,
                        prf: KeyDerivationPrf.HMACSHA256,
                        iterationCount: 100000,
                        numBytesRequested: 256 / 8)),
                    PositionId = positions[0].Id
                },
                new Administrator
                {
                    Id = Guid.NewGuid(),
                    Email = "nguyendat012002@icloud.com",
                    PasswordHash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                        password: "P@ssw000000rd",
                        salt: salt,
                        prf: KeyDerivationPrf.HMACSHA256,
                        iterationCount: 100000,
                        numBytesRequested: 256 / 8)),
                    PositionId = positions[1].Id
                }
            };

            var rates = new Rate[]
            {
                new Rate
                {
                    Id = Guid.NewGuid(),
                    Rating = 4.5,
                    Header = "Good Product",
                    Content = "The beauty product exceeded my expectations! It left my skin feeling incredibly smooth and radiant. The pleasant scent was an added bonus. I highly recommend this product for anyone seeking effective and enjoyable skincare.",
                    CreatedDate = DateTime.Now,
                    ProductId = products[0].Id,
                    UserId = users[1].Id
                },
                new Rate
                {
                    Id = Guid.NewGuid(),
                    Rating = 4.8,
                    Header = "Excellent!!!",
                    Content = "This beauty product is a game-changer! I've noticed a remarkable improvement in my skin's texture and appearance. It's lightweight and absorbs quickly, making it perfect for daily use. I'm thrilled with the results and will definitely repurchase.",
                    CreatedDate = DateTime.Now,
                    ProductId = products[0].Id,
                    UserId = users[1].Id
                },
                new Rate
                {
                    Id = Guid.NewGuid(),
                    Rating = 4.8,
                    Header = "Recommended Product",
                    Content = "I can't get enough of this beauty product! It's become my holy grail. My skin feels rejuvenated and the product imparts a natural glow. The packaging is elegant and the results speak for themselves. A true gem in the world of beauty.",
                    CreatedDate = DateTime.Now,
                    ProductId = products[1].Id,
                    UserId = users[1].Id
                },
                new Rate
                {
                    Id = Guid.NewGuid(),
                    Rating = 4.6,
                    Header = "Very good",
                    Content = "I'm absolutely thrilled with this beauty product. It has transformed my skincare routine. The formula feels luxurious and has visibly reduced fine lines. My friends have been asking about my secret! A must-have for anyone looking to enhance their beauty regimen.",
                    CreatedDate = DateTime.Now,
                    ProductId = products[2].Id,
                    UserId = users[1].Id
                },
            };

            builder.Entity<Position>().HasData(positions);
            builder.Entity<Administrator>().HasData(admins);
            builder.Entity<Company>().HasData(companies);
            builder.Entity<Product>().HasData(products);
            builder.Entity<Category>().HasData(categories);
            builder.Entity<IdentityRole<Guid>>().HasData(roles);
            builder.Entity<ApplicationUser>().HasData(users);
            builder.Entity<Order>().HasData(orders);
            builder.Entity<ProductOrder>().HasData(pos);
            builder.Entity<ProductUser>().HasData(pus);
            builder.Entity<SpaTreatmentPlan>().HasData(stps);
            builder.Entity<IdentityUserRole<Guid>>().HasData(iurs);
            builder.Entity<Report>().HasData(reports);
            builder.Entity<Technology>().HasData(techlonogies);
            builder.Entity<ProductTechnology>().HasData(productTechlonogies);
            builder.Entity<CompanyTechnology>().HasData(companiesTechnology);
            builder.Entity<Privacy>().HasData(privaci);
            builder.Entity<Rate>().HasData(rates);
        }
    }
}


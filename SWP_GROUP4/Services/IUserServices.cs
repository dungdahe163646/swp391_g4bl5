﻿using System;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.Services
{
	public interface IUserServices
	{
        bool IsEmailExist(string Email);
        Task<string> GenerateResetPasswordToken(Guid userId);
        ApplicationUser GetUserByEmail(string Email);
        bool IsResetPasswordTokenNullOrExpire(string resetPasswordToken);
        bool IsOTPNullOrExpire(string OTP);
        Task<string> ResetPassword(Guid userId);
        ApplicationUser GetUserByResetPasswordToken(string resetPasswordToken);
        Task ChangePassword(Guid userId, string newPassword);
    }
}


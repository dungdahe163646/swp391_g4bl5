﻿using System;
using System.Net;
using System.Net.Mail;

namespace SWP_GROUP4.Services
{
	public class EmailServices : IEmailServices
    {
        private readonly IConfiguration _config;

        public EmailServices(IConfiguration config)
		{
            _config = config;
        }

        public void Send(string title, string content, string to)
        {
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress(_config["Email:Account"]);
            message.To.Add(new MailAddress(to));
            message.Subject = title;
            message.Body = content;
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            smtp.Credentials = new NetworkCredential(_config["Email:Account"], _config["Email:Password"]);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Send(message);
        }
    }
}


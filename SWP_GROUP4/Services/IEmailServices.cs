﻿using System;
namespace SWP_GROUP4.Services
{
	public interface IEmailServices
	{
        void Send(string title, string content, string to);
    }
}


﻿using System;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NuGet.Common;
using SWP_GROUP4.DbContexts;
using SWP_GROUP4.Models;

namespace SWP_GROUP4.Services
{
	public class UserServices : IUserServices   
	{
        private readonly FBeautyDbContext _context;
        public UserServices(FBeautyDbContext context)
        {
            _context = context;
        }

        public bool IsEmailExist(string Email)
        {
            return _context.Users.Any(u => u.Email == Email);
        }

        public async Task<string> GenerateResetPasswordToken(Guid userId)
        {
            ApplicationUser user = _context.Users.Find(userId);
            user.ResetPasswordTokenExpireAt = DateTime.Now.AddMinutes(5);
            using Aes crypto = Aes.Create();
            crypto.GenerateKey();
            user.ResetPasswordToken = Convert.ToBase64String(crypto.Key);
            await _context.SaveChangesAsync();
            return user.ResetPasswordToken;
        }

        public ApplicationUser GetUserByEmail(string Email)
        {
            var user = _context.Users.FirstOrDefault(user => user.Email == Email);
            if (user == null)
                return null;
            return user;
        }

        public bool IsResetPasswordTokenNullOrExpire(string resetPasswordToken)
        {
            var user = _context.Users.FirstOrDefault(user => user.ResetPasswordToken == resetPasswordToken);
            if (user != null && DateTime.Now < user.ResetPasswordTokenExpireAt)
                return false;
            return true;

        }


        public async Task<string> ResetPassword(Guid userId)
        {
            var hasher = new PasswordHasher<ApplicationUser>();
            var user = _context.Users.Find(userId);
            string tempPassword = GenerateRandomPassword();
            user.PasswordHash = hasher.HashPassword(null, tempPassword);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
            return tempPassword;
        }

        private string GenerateRandomPassword()
        {
            var rd = new Random();
            char[] randoms = new char[10];
            randoms = randoms.Select(e => (char)rd.Next(97, 123)).ToArray();
            return new string(randoms);
        }

        public ApplicationUser GetUserByResetPasswordToken(string token)
        {
            var user = _context.Users.FirstOrDefault(user => user.ResetPasswordToken == token);
            if (user == null)
                return null;
            //var roles = _context.UserRoles.Include(ur => ur.Role).Where(ur => ur.UserId == user.Id).Select(ur => ur.Role.Name);
            return user;
        }

        public bool IsOTPNullOrExpire(string OTP)
        {
            var user = _context.Users.FirstOrDefault(user => user.OTP == OTP);
            if (user != null && DateTime.Now < user.OTPExpireAt)
                return false;
            return true;
        }

        public async Task ChangePassword(Guid userId, string newPassword)
        {
            var hasher = new PasswordHasher<ApplicationUser>();
            var user = _context.Users.Find(userId);
            user.PasswordHash = hasher.HashPassword(null, newPassword);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }
    }
}

